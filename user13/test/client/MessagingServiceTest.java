package client;

import static org.junit.Assert.*;
import helperClasses.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class MessagingServiceTest {

	private static final String middlewareHost = "localhost";
	private static final int middlewarePort = 3456;
	private static MessagingServiceImpl service = null;
	private static List<Integer> queueList = new ArrayList<Integer>();
	private static List<Integer> clientList = new ArrayList<Integer>();
	
	public static void assertListContains(Integer expected, List<Integer> actual) {
		for (Integer x:actual) {
			if (x == expected)
				return;
		}
		fail("Could not find element " + expected.toString() + " in  the list");
	}
	
	public static void assertListElements(List<Integer> expected, List<Integer> actual) {
		assertNotNull(actual);
		assertEquals("Size of expected and actual list don't match",expected.size(), actual.size());
		for (Integer e:expected) {
			assertListContains(e, actual);
		}
	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Setting up the test here");
		Socket middlewareSocket = new Socket(middlewareHost, middlewarePort);
		PrintWriter out = new PrintWriter(middlewareSocket.getOutputStream(),
				true);
		BufferedReader in = new BufferedReader(new InputStreamReader(
				middlewareSocket.getInputStream()));
		service = new MessagingServiceImpl(out, in, middlewareSocket);
		
		// Create 1 User 
		int firstClient = service.register();
		clientList.add(firstClient);
		assertListElements(clientList, service.listClients());
		// Create 3 queues
		assertListElements(queueList, service.listQueues());
		queueList.add(service.createQueue());
		assertListElements(queueList, service.listQueues());
		queueList.add(service.createQueue());
		assertListElements(queueList, service.listQueues());
		queueList.add(service.createQueue());
		assertListElements(queueList, service.listQueues());
		System.out.println("Exiting setUpBeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass begins");
		// Delete the 3 queues created during setup
		
		for (Integer qid : queueList) {
			service.deleteQueue(qid);
		}
		queueList.clear();
		assertListElements(queueList, service.listQueues());
		
		// Remove the client
		service.deregister();
		System.out.println("tearDownAfterClass ends");
	}

	
	@Before
	public void setUp() throws Exception {
				
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public final void testMethod() {
		System.out.println("testMethod being run");
		assertEquals("Numbers not equal", 20, 20);
		System.out.println("testMethod still running");
		//assertEquals("Numbers not equal again", 23, 24);
	}
	
	
	@Test
	public final void testCreateAndDeleteQueue() throws IOException {
	
		
		queueList.add(service.createQueue());
		assertListElements(queueList, service.listQueues());
		
		queueList.add(service.createQueue());
	    assertListElements(queueList, service.listQueues());
		
		service.deleteQueue(queueList.get(4));
		queueList.remove(4);
		assertListElements(queueList, service.listQueues());
		service.deleteQueue(queueList.get(3));
		queueList.remove(3);
		assertListElements(queueList, service.listQueues());
	}
	
	@Test
	public final void testSendGetAndReadFromDeletedQueue() throws IOException {
		queueList.add(service.createQueue());
		assertListElements(queueList, service.listQueues());
		int qid = queueList.get(3);
		service.deleteQueue(qid);
		queueList.remove(3);
		assertListElements(queueList, service.listQueues());
		
		Message msg = new Message(clientList.get(0), null, qid, 10, "Test Message");
		service.sendMessage(msg);
		
		Integer context = null;
		msg = service.readMessage(qid, context, true, null);
		assertNull(msg);
		msg = service.readMessage(qid, context, false, null);
		assertNull(msg);
		msg = service.getMessage(qid, context, true, null);
		assertNull(msg);
		msg = service.getMessage(qid, context, false, null);
		assertNull(msg);
		
	}
	/*

	@Test
	public final void testSendMessageIntegerIntegerIntegerListOfInteger() {
		fail("Not yet implemented"); // TODO
	}
	*/
	
	@Test
	public final void testSendQueueReadAndGetPriorityTime() throws IOException {
		boolean byTimeStamp = false;
		for (int k = 0; k < 2; k++) {
			// Insert 3 messages on each queue
			Integer senderID = clientList.get(0);
			Integer receiverID = null;
			Integer context = null;
			byTimeStamp = !byTimeStamp; // Toggle byTimeStampFlag
			
			int priority = 10;
			for (int qid:queueList) {
				for (int i = 0; i < 3; i++) {
						String text = "Hello" + Integer.toString(i) + " " + Integer.toString(qid);
						priority -= 1;
						Message msg = new Message(senderID, receiverID, qid, priority, text);
						service.sendMessage(msg);
					}
			}
	
			// Read messages
			Integer senderOfMessage = null;
			for (int qid:queueList) {
				String expectedText = "Hello" + Integer.toString(0) + " " + Integer.toString(qid);
				for (int i = 0; i < 3; i++) {
					Message msg = service.readMessage(qid, context, byTimeStamp, senderOfMessage);
					assertNotNull(msg);
					assertEquals(expectedText, msg.getText());
				}
			}
	
			// Get messages (that is, Remove messages from the database as well)
			for (int qid:queueList) {
				for (int i = 0; i < 3; i++) {
					String expectedText = "Hello" + Integer.toString(i) + " " + Integer.toString(qid);			
					Message msg = service.getMessage(qid, context, byTimeStamp, senderOfMessage);
					assertEquals(expectedText, msg.getText());
				}
			}
			
			// Now, the queues are empty. Read or get Message
			// should return null
			for (int qid:queueList) {
				for (int i = 0; i < 3; i++) {
					Message msg = service.readMessage(qid, context, byTimeStamp, senderOfMessage);
					assertNull(msg);
					msg = service.getMessage(qid, context, byTimeStamp, senderOfMessage);
					assertNull(msg);
				}
			}
		}
	}

	@Test
	public final void testSendQueueReceiverReadAndGetTime() throws IOException {
		// Insert 3 messages on each queue
		
		Integer senderID = clientList.get(0);
		boolean byTimeStamp = true;
		Integer context = null;
		
		Random ran = new Random();
		List<Integer> receivers = service.listClients();
    	Integer receiverID = receivers.get(ran.nextInt(receivers.size()));
		
		int priority = 0;
		for (int qid:queueList) {
			for (int i = 0; i < 3; i++) {
					String text = "Hello" + Integer.toString(i) + " " + Integer.toString(qid);
					priority += 1;
					Message msg = new Message(senderID, receiverID, qid, priority, text);
					service.sendMessage(msg);
				}
		}

		// Read messages
		Integer senderOfMessage = null;
		for (int qid:queueList) {
			String expectedText = "Hello" + Integer.toString(0) + " " + Integer.toString(qid);
			for (int i = 0; i < 3; i++) {
				Message msg = service.readMessage(qid, context, byTimeStamp, senderOfMessage);
				assertEquals(expectedText, msg.getText());
			}
		}

		// Get messages (that is, Remove messages from the database as well)
		for (int qid:queueList) {
			for (int i = 0; i < 3; i++) {
				String expectedText = "Hello" + Integer.toString(i) + " " + Integer.toString(qid);			
				Message msg = service.getMessage(qid, context, byTimeStamp, senderOfMessage);
				assertEquals(expectedText, msg.getText());
			}
		}
		
		// Now, the queues are empty. Read or get Message
		// should return null
		for (int qid:queueList) {
			for (int i = 0; i < 3; i++) {
				Message msg = service.readMessage(qid, context, byTimeStamp, senderOfMessage);
				assertNull(msg);
				msg = service.getMessage(qid, context, byTimeStamp, senderOfMessage);
				assertNull(msg);
			}
		}
	}
	
	@Test
	public final void testSendMulQueuesReadAndGetTime() throws IOException {
		Integer senderId = clientList.get(0);
		Integer receiverId = null;
		boolean by_priority = false;
		Integer context = null;
		
		int priority = 1;
		String text = "Abracadabra";
		Message msg = new Message(senderId, receiverId, queueList, priority, text);
		System.out.println("Calling sendMessage now to send to a list of queues");
		service.sendMessage(msg);
		System.out.println("Message sent to list of queues");
		Integer senderOfMessage = null;
		for (int qid:queueList) {
			msg = service.readMessage(qid, context, by_priority, senderOfMessage);
			assertEquals(text, msg.getText());
			msg = service.getMessage(qid, context, by_priority, senderOfMessage);
			assertEquals(text, msg.getText());
			msg = service.getMessage(qid, context, by_priority, senderOfMessage);
			assertNull(msg);
		}
	}
	
	@Test
	public final void testSendQueueReceiverReadAndGetSenderTime() throws IOException {
		// Insert 3 messages on each queue
		
		Integer senderID = clientList.get(0);
		boolean byTimeStamp = true;
		Integer context = null;
		
		Random ran = new Random();
		List<Integer> receivers = service.listClients();
    	Integer receiverID = receivers.get(ran.nextInt(receivers.size()));
		
		int priority = 0;
		int qid = queueList.get(0);
		for (int i = 0; i < 3; i++) {
				String text = "Hello" + Integer.toString(senderID);
				priority += 1;
				Message msg = new Message(senderID, receiverID, qid, priority, text);
				assertEquals(service.sendMessage(msg), true);
		}

		// Read messages
		Integer senderOfMessage = senderID;
			
		String expectedText = "Hello" + Integer.toString(senderID);
		for (int i = 0; i < 3; i++) {
				Message msg = service.readMessage(null, context, byTimeStamp, senderOfMessage);
				assertEquals(expectedText, msg.getText());
		}
		// Get messages (that is, Remove messages from the database as well)
		
		for (int i = 0; i < 3; i++) {
			expectedText = "Hello" + Integer.toString(senderID);			
			Message msg = service.getMessage(null, context, byTimeStamp, senderOfMessage);
			assertEquals(expectedText, msg.getText());
		}
		
		
		// Now, the queues are empty. Read or get Message
		// should return null
			for (int i = 0; i < 3; i++) {
				Message msg = service.readMessage(qid, context, byTimeStamp, senderOfMessage);
				assertNull(msg);
				msg = service.getMessage(qid, context, byTimeStamp, senderOfMessage);
				assertNull(msg);
			}
	}
	
	
	
	
	
	
	@Ignore
	public final void testListClients() throws IOException {
		// Add a client
		clientList.add(service.register());
		assertListElements(clientList, service.listClients());
		
		// Remove the client
		service.deregister();
		clientList.remove(1);
		assertListElements(clientList, service.listClients());
	}
	
	/*
	@Test
	public final void testQueryQueuesInt() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testQueryQueues() {
		fail("Not yet implemented"); // TODO
	}
	*/

}
