DROP TABLE IF EXISTS client, queue, message CASCADE;
DROP FUNCTION IF EXISTS createClient(OUT integer);
DROP FUNCTION IF EXISTS deleteClient(integer);
DROP FUNCTION IF EXISTS createQueue(OUT integer);
DROP FUNCTION IF EXISTS deleteQueue(integer);
DROP FUNCTION IF EXISTS insertMessageOne(integer,integer,integer,integer,integer,character varying);
DROP FUNCTION IF EXISTS insertMessageMany(integer,integer,integer[],integer,integer,character varying);

DROP FUNCTION IF EXISTS readMessage(integer, integer, integer, integer, boolean);
DROP FUNCTION IF EXISTS deleteMessage(integer);
DROP FUNCTION IF EXISTS listQueue();
DROP FUNCTION IF EXISTS listQueues(integer);
DROP FUNCTION IF EXISTS listUsers();

CREATE TABLE IF NOT EXISTS client (
	id serial primary key
);

CREATE TABLE IF NOT EXISTS queue (
	id serial primary key 
);

CREATE TABLE IF NOT EXISTS message (
	id serial primary key,
	sender_id int NOT NULL,
	receiver_id int DEFAULT NULL references client(id) ON DELETE CASCADE,
	queue_id int  NOT NULL references queue(id) ON DELETE CASCADE,
	context int DEFAULT 0,
	priority int DEFAULT 1 NOT NULL,
	arrive_time timestamp DEFAULT current_timestamp,
	text varchar(2000) NOT NULL
);

/* Explicit indexes */
DROP INDEX IF EXISTS arrive_time_idx;
DROP INDEX IF EXISTS priority_idx;
DROP INDEX IF EXISTS sender_idx;
DROP INDEX IF EXISTS queue_idx;
DROP INDEX IF EXISTS receiver_idx;
DROP INDEX IF EXISTS context_idx;
CREATE INDEX arrive_time_idx ON message (arrive_time);
CREATE INDEX priority_idx ON message (priority);
CREATE INDEX sender_idx ON message (sender_id);
CREATE INDEX queue_idx ON message (queue_id);
CREATE INDEX receiver_idx ON message (receiver_id);
CREATE INDEX context_idx ON message (context);

/* Functions */

CREATE OR REPLACE FUNCTION createClient(OUT id   INT)
RETURNS INT 
AS $$
INSERT INTO client values(default) RETURNING id;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION deleteClient(id INT)
RETURNS INT 
AS $$
DELETE FROM client WHERE id=$1 RETURNING id;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION createQueue(OUT id   INT)
RETURNS INT
AS $$
INSERT INTO queue values(default) RETURNING id;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION deleteQueue(id INT = NULL)
RETURNS INT
AS $$
DELETE FROM message WHERE queue_id=$1 ;
DELETE FROM queue WHERE id=$1 RETURNING id;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION insertMessageOne(sender_id INT, receiver_id INT, queue_id INT, context INT, priority INT, text varchar(2000))
RETURNS BOOLEAN 
AS $$
DECLARE
	start timestamp;
	stop timestamp;
BEGIN
    start = clock_timestamp();
    INSERT INTO message(sender_id, receiver_id, queue_id, context, priority, text) VALUES($1,$2,$3,$4,$5,$6);
    stop = clock_timestamp();
    RAISE NOTICE 'senderId: (%) insertMessageOne: (%)', sender_id, stop - start;
    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE 'EXCEPTION: INSERT SINGLE-Q MESSAGE FAILED';
        RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insertMessageMany(sender_id INT, receiver_id INT, queue_ids integer[], context INT, priority INT, text varchar(2000))
RETURNS BOOLEAN
AS $$
DECLARE
   q INT;
BEGIN
FOREACH q IN ARRAY $3 LOOP
		INSERT INTO message(sender_id, receiver_id, queue_id, context, priority, text) VALUES ($1, $2, q, $4, $5, $6);
END LOOP;
RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE 'EXCEPTION: INSERT MULTI-Q MESSAGE FAILED';
        RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION readOrGetMessage(messageFor INT, qid INT = NULL, ctxt INT = NULL, messageFrom INT = NULL, byTimeStamp BOOLEAN = FALSE, remove BOOLEAN = FALSE)
RETURNS SETOF message
AS $$
DECLARE
	sql varchar;
	start timestamp;
	stop timestamp;
BEGIN

	start := clock_timestamp();	
	sql := 'SELECT';

	IF remove THEN
		sql := 'DELETE FROM message WHERE id in (' || sql || ' id';
	ELSE
		sql := sql || ' *';
	END IF;
		
	sql := sql || ' FROM message WHERE (receiver_id IS NULL OR receiver_id = $1)';
	IF qid IS NOT NULL THEN
		sql := sql || ' AND queue_id = $2';
	END IF;
	IF ctxt IS NOT NULL THEN
		sql := sql || ' AND context = $3';
	END IF;

	IF $4 IS NOT NULL THEN
		sql := sql || ' AND sender_id = $4';
	END IF;

	sql := sql || ' ORDER BY';
	IF byTimeStamp THEN
		sql := sql || ' arrive_time ASC';
	ELSE
		sql := sql || ' priority DESC';
	END IF;

	sql := sql || ' LIMIT 1'; 
	
	IF remove THEN
		sql := sql || ') RETURNING *';
	END IF;

	RETURN QUERY EXECUTE sql
	USING messageFor, qid, ctxt, messageFrom;

	stop = clock_timestamp();
	RAISE NOTICE 'senderId: (%) readOrGetMessage: (%)', messageFor, stop - start;
		
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION deleteMessage(id INT = NULL)
RETURNS INT
AS $$
DELETE FROM message WHERE id=$1 RETURNING id;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION listQueue()
RETURNS TABLE (
	id INT
)
AS $$
SELECT id FROM queue;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION listQueues(id INT)
RETURNS TABLE(
	id INT
)
AS $$
SELECT DISTINCT queue_id FROM message WHERE (receiver_id=$1 OR receiver_id IS NULL);
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION listUsers()
RETURNS TABLE(
	id INT
)
AS $$
SELECT id FROM client;
$$ LANGUAGE SQL;
