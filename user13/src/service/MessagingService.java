package service;

import helperClasses.Message;

import java.util.List;

public interface MessagingService {
	
	/*
	 * Registers the calling client with the
	 * database. Database returns a unique ID
	 * for the client.
	 * 
	 * @return A unique integer Id for the calling client
	 */
	public Integer register();
	
	/*
	 * Removes the unique ID of the calling client
	 * from the database.
	 * 
	 * @return None
	 */
	public void deregister();
	
	/*
	 * Creates a queue in the database
	 * 
	 * @return Returns a unique ID for the queue
	 */
	public Integer createQueue();
	
	/*
	 * Deletes a queue with specified queue ID
	 * 
	 * @param queueID ID of the queue to be deleted
	 * @return Boolean value indicating if the deletion
	 * 			of queue was successful. True for success,
	 * 			False for failure.
	 */
	public Boolean deleteQueue(int queueID);
	
	/*
	 * Send a message to a queue (in the database). All the required parameters 
	 * to identify the destination of the message must be specified
	 * as Message fields
	 * 
	 * @param msg Message to be sent
	 * @return Boolean value indication if the operation was successful.
	 * 		   True for success, False for failure.
	 */
	public Boolean sendMessage(Message msg);
	
	/*
	 * Read a message from the database that meets the criteria
	 * specified in the parameters. In particular, the message
	 * read is not deleted from the database.
	 * 
	 * @param queueID ID of the queue from which the message must be read
	 * @param context context ID of the message (may be NULL)
	 * @param byTimeStamp A Boolean value indicating if the 'earliest' message
	 * 						should be read. If false, the message with the highest priority
	 * 						is read instead.
	 * @param senderOfMessage	An integer specifying the ID of the client that must be the sender of the
	 * 							message to be read. Allows clients to query for a message from a particular
	 * 							sender. 
	 */
	public Message readMessage(Integer queueID, Integer context, Boolean byTimeStamp, Integer senderOfMessage);
	
	/*
	 * Retrieves a message from the database that meets the criteria 
	 * specified in the parameters. In particular, the message 
	 * retrieved is deleted from the database.
	 * 
	 *  @param	queueID ID of the queue from which the message must be retrieved
	 *  @param 	context Context ID of the message (may be NULL)
	 *  @param 	byTimeStamp A Boolean value indicating if the 'earliest' message
	 * 						should be read. If false, the message with the highest priority
	 * 						is read instead.
	 * 	@param senderOfMessage	An integer specifying the ID of the client that must be the sender of the
	 * 							retrieved message. Allows clients to query for a message from a particular
	 * 							sender.
	 * 
	 * @return Message that matches the criteria specified in the parameters. 
	 */
	public Message getMessage(Integer queueID, Integer context, Boolean byTimeStamp, Integer senderId);
	
	/*
	 * Returns a list of clients that exist in the system. 
	 * 
	 * @return A list of integers (unique client IDs) that identify all the clients 
	 * 		   registered in the system
	 */
	public List<Integer> listClients();
	
	/*
	 * Returns a list of queueIDs that identify the queues holding a message 
	 * for the client specified as a parameter.
	 * 
	 * @param receiverID ID of the client that must be the receiver of message  
	 *					 in the queues.
	 * @return A list of integers identifying queues with a message for the client
	 * 		   specified as a parameter. An empty list is returned if no such 
	 * 		   messages exist in the system.
	 */
	public List<Integer> listQueues(int receiverID);
	
	/*
	 * Returns a list of queueIDs identifying all the queues in the system.
	 * 
	 * @return A list of Integers, where each Integer identifies a queue in
	 * 		   the system.
	 */
	public List<Integer> listQueues();
}
