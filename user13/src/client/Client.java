package client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Logger;

import service.MessagingService;
import logging.RequestTracer;
import helperClasses.Constants;

public class Client implements Constants, Runnable {

	protected Integer ID;
	protected Integer clientType;
	protected Socket middlewareSocket;
	protected PrintWriter out;
	protected BufferedReader in;
	protected long endTime;
	protected MessagingService service;
	protected Logger lgr = null;
	protected Boolean tracing;
	protected long waitInterval;
	protected long pollInterval;
	protected static final String logDir = System.getProperty("java.io.tmpdir");
	
	public Client(InetAddress middlewareIP, int middlewarePort,
			long timeRunning, Boolean tracing, long waitInterval, long pollInterval) throws IOException {
			this.middlewareSocket = new Socket(middlewareIP, middlewarePort);
			this.out = new PrintWriter(this.middlewareSocket.getOutputStream(),
					false);
			this.in = new BufferedReader(new InputStreamReader(
					this.middlewareSocket.getInputStream()));
			this.service = new MessagingServiceImpl(this.out, this.in, this.middlewareSocket);
			this.ID = service.register();
			this.tracing = tracing;
			if (tracing) {
				this.service = new RequestTracer(this.service);
			}
			this.waitInterval = waitInterval;
			this.pollInterval = pollInterval;
			this.endTime = System.currentTimeMillis() + timeRunning;
			if (tracing) {
				((RequestTracer)service).setFileLog(new BufferedWriter(new FileWriter(new File(logDir + File.separator + "clientThread" + this.ID))));
			}
			lgr = Logger.getLogger(Client.class.getName() + this.ID);
			lgr.info("Client " + this.ID + " starting up...");
	}

	public Integer getClientID() {
		return this.ID;
	}
	
	public MessagingService getService() {
		return this.service;
	}
	
	public static void main(String args[]) {
	
	}

	@Override
	public void run() {
		
	}
	
	protected boolean runLongEnough() {
		return System.currentTimeMillis() > endTime;
	}	
	
}
