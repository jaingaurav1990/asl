package client;

import helperClasses.Convertor;
import helperClasses.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;

import service.MessagingService;

public class MessagingServiceImpl implements MessagingService {

	private PrintWriter out;
	private BufferedReader in;
	private Socket middlewareSocket;
	
	MessagingServiceImpl(PrintWriter out, BufferedReader in, Socket middlewareSocket) {
		this.out = out;
		this.in = in;
		this.middlewareSocket = middlewareSocket;
	}
	
	@Override
	public Integer register() {
		try {
			out.println("register");
			out.flush();
			String status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)) {
				/*
				 * If even this basic operation fails, Client should 
				 * also fail
				 */
				throw new RuntimeException(); 
			} 
			else {
				HashMap<String, String> query = Convertor
						.stringToMap(in.readLine());
				return Integer.valueOf(query.get("id"));
			}
		}
		catch (IOException e) {
			/*
			 * We always expect the server to serve this
			 * request without any exceptions being raised.
			 * Wrap and throw an unchecked exception
			 */
			throw new RuntimeException(e);
		}
	}

	@Override
	public void deregister() {
		try {
			out.println("deregister");
			out.flush();
			if (!(in.readLine()).equals(helperClasses.Constants.OK)) {
				throw new RuntimeException();
			}
			tearDown();
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void tearDown() throws IOException {
		this.out.close();
		this.in.close();
		this.middlewareSocket.close();
	}

	@Override
	public Integer createQueue() {
		out.println("create");
		out.flush();
		String status = "";
		try {
			status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)) {
				/*
				 * Creating a queue in the database should
				 * always succeed. Otherwise client should 
				 * just fail and stop.
				 */
				throw new RuntimeException();
			} else {
				HashMap<String, String> query = Convertor
						.stringToMap(in.readLine());
				int queueID = Integer.valueOf(query.get("id"));
				return queueID;
			}
		} catch (IOException e) {
			/*
			 * Something went terribly wrong. Wrap this
			 * exception into an unchecked exception and
			 * throw it. If tracing is on, this exception
			 * will be logged as well by RequestTracer
			 */
			throw new RuntimeException(e);
		}
	}

	@Override
	public Boolean deleteQueue(int queueID) {
		try {
			out.println("delete");
			out.println("id:" + queueID);
			out.flush();
			String status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)) {
				System.out.println("Deleting queue failed");
				return false;
			}
			else {
				return true;
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Boolean sendMessage(Message msg) {		
		try {
			out.println("send");
			out.println(Convertor.messageToString(msg));
			out.flush();
			String status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)){
				return false;
			}
			else {
				return true;
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Message readMessage(Integer queueID, Integer context,
			Boolean byTimeStamp, Integer senderOfMessage) {
		try {
			String queryString = "arrive_time_order:" + byTimeStamp;
			if(queueID != null)
				queryString += ";queue:" + queueID;
			if(context != null)
				queryString += ";context:" + context;
			if(senderOfMessage != null)
				queryString += ";sender:" + senderOfMessage;
			out.println("read");
			out.println(queryString);
			out.flush();
			String status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)) {
				return null;
			} else {
				HashMap<String, String> query = Convertor
						.stringToMap(in.readLine());
				return Convertor.mapToMessage(query);	
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Message getMessage(Integer queueID, Integer context,
			Boolean byTimeStamp, Integer senderId) {
		try {
	        String queryString = "arrive_time_order:" + byTimeStamp;
			if(queueID != null)
				queryString += ";queue:" + queueID; 
			if(context != null)
				queryString += ";context:" + context;
			if(senderId != null)
				queryString += ";sender:" + senderId;
			out.println("get");
			out.println(queryString);
			out.flush();
			String status = in.readLine();
			if (helperClasses.Constants.OK.equals(status)) {
				HashMap<String, String> query = Convertor
						.stringToMap(in.readLine());
				//lgr.log(STAT_MSG, query.get("message"));
				return Convertor.mapToMessage(query);
			}
			else {
				return null;
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Integer> listClients() {
		try {
			out.println("users");
			out.flush();
			String status = in.readLine();
			if(helperClasses.Constants.OK.equals(status)) {
				HashMap<String, String> query = Convertor.stringToMap(in.readLine());
				return Convertor.stringToList(query.get("users"));
			}
			else {
				return null;
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Integer> listQueues(int receiverID) {
		try {
			out.println("list"); 
			out.println("receiver:" + receiverID);
			out.flush();
			String status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)) {
				return null;
			} else {
				HashMap<String, String> query = Convertor
						.stringToMap(in.readLine());
				return Convertor.stringToList(query.get("queues"));
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Integer> listQueues() {
		try {
			out.println("list");
			out.println("receiver:-1");
			out.flush();
			String status = in.readLine();
			if (!helperClasses.Constants.OK.equals(status)) {
				return null;
				
			} else {
				HashMap<String, String> query = Convertor
						.stringToMap(in.readLine());
				List<Integer> list = Convertor.stringToList(query.get("queues"));
				return list;
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}

	}
}
