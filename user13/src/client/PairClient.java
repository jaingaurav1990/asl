package client;

import helperClasses.Message;

import java.io.IOException;
import java.net.InetAddress;
import logging.RequestTracer;

public class PairClient extends Client {
	
	private int partner = -1;
	private boolean requester = false;
	private Integer readQ = -1;
	private Integer writeQ = -1;

	public PairClient(InetAddress middlewareIP, int middlewarePort,
			long timeRunning, int readQ, int writeQ, int partner, boolean requester, Boolean tracing, long waitInterval,
			long pollInterval) throws IOException {
		super(middlewareIP, middlewarePort,timeRunning, tracing, waitInterval, pollInterval);
		this.partner = partner;
		this.requester = requester;
		this.readQ = readQ;
		this.writeQ = writeQ;

		if (tracing) {
			((RequestTracer)service).getFileLog().write("Client " + this.ID + " started with parameters " 
					+ "middlewareIP: " + middlewareIP + " middlewarePort:" + middlewarePort 
					+ " readQ: " + readQ + " writeQ: " + writeQ  
					+ " partner: " + partner + " timeRunning: " + timeRunning + " tracing: " + tracing
					+ " waitInterval: " + waitInterval + " pollInterval: " + pollInterval
					+ "\n");
	
		}
	}
	
	private void sendInitialMessage() {
		int priority = 5; 
		Message msg = new Message(this.ID, partner, writeQ, priority, "0");
		if (!service.sendMessage(msg)) {
			throw new RuntimeException();
		}
	}
	
	public void run() {
		try {
	
			/*
			 * Following assignment of writeQ and readQ
			 * are appropriate only if this client is a
			 * requester
			 */
			int expected = 1;

			Message msg = null;
			
			if(requester) {
				sendInitialMessage();
			}
			else {
				expected = 0;
			}
		
			while(!runLongEnough()) {
				
					long elapsedTime = ((RequestTracer)service).getElapsedTime();
					do {
						msg = service.getMessage(readQ, null, false, partner);
						if (tracing) {
							elapsedTime += ((RequestTracer)service).getElapsedTime();
						}

						if (msg != null) {
							break;
						}
						else {
							elapsedTime += pollInterval;
							Thread.sleep(pollInterval);
						}
					} while(msg == null && !runLongEnough());
					
					long t1 = System.currentTimeMillis();
					if (msg == null) {
						System.out.println(this.ID + " time up. Terminating..");
						service.deregister();
						return;
				    }
					
					Integer receivedInt = Integer.parseInt(msg.getText());
					if (receivedInt != expected) {
						((RequestTracer)service).getFileLog().write("Mismatch between expected and received text."
								+ " Raising runtime Exception");
						service.deregister();
						throw new RuntimeException();
					}

					expected += 2;
					msg.setMessage(Integer.toString(receivedInt + 1));
					msg.setsenderID(this.ID);
					msg.setQueueID(writeQ);
					msg.setReceiverID(partner);
					
					elapsedTime += (System.currentTimeMillis() - t1);
			    	long thinkTime = (waitInterval - elapsedTime);
			    	if (tracing) {
						((RequestTracer)service).getFileLog().write(System.currentTimeMillis() + ": " + "thinktime: " + thinkTime + "\n");
					}
					if (requester && thinkTime > 0) {
					    Thread.sleep(thinkTime);
					}
					if (!service.sendMessage(msg)) {
						System.out.println(this.ID + " failed to send a message. Terminating..");
						service.deregister();
						return;
					}
			}
			
			service.deregister();
		} catch (InterruptedException e) {
			lgr.log(ERROR, e.getMessage(), e);
		}
        catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        }
}
