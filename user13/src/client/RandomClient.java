package client;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import logging.RequestTracer;
import helperClasses.Message;

    public class RandomClient extends Client{
    private Integer startId ;
    private Integer endId ;
	private Integer queueId;
	private Integer msgLen;
	private List<Integer> randomClientList = new ArrayList<Integer>();

	public RandomClient(InetAddress middlewareIP,
			int middlewarePort, int qid, long timeRunning, Boolean tracing, long waitInterval, long pollInterval, 
			int startId, int endId, int messageLength) throws IOException {
		super(middlewareIP, middlewarePort, timeRunning, tracing, waitInterval, pollInterval);
		this.queueId = qid;
		this.startId = startId;
		this.endId = endId;
		if (tracing) {
			((RequestTracer)service).getFileLog().write("Client " + this.ID + " started with parameters " 
					+ "middlewareIP: " + middlewareIP + " middlewarePort:" + middlewarePort 
					+ "qid: " + qid + " timeRunning: " + timeRunning + " tracing: " + tracing
					+ " waitInterval: " + waitInterval + " pollInterval: " + pollInterval
					+ " startId: " + startId + " endId: " + endId + "\n");
		}

		for (Integer c = this.startId; c <= this.endId; c++) {
			this.randomClientList.add(c);
		}
		this.randomClientList.remove(this.ID);
		this.msgLen = messageLength;
	}
	
	protected void sendInitialMessage() {
		Random ran = new Random();
		
	    if(randomClientList.size() < 2){
	    	System.out.println(this.ID + " size is not enough , Terminating..");
			service.deregister();
			return;
	    }
	    	
	    /*
	     * Create a string of length specified by the message length
	     */
	    char[] charArray = new char[msgLen];
	    Arrays.fill(charArray, '0');
	    String text = new String(charArray);
	    System.out.println("Passing around " + text);
	    Integer receiver = randomClientList.get(ran.nextInt(randomClientList.size()));
		Message msg = new Message(this.ID, receiver, queueId, null, 5, text);
		service.sendMessage(msg);
			
	}
	
	@Override
	public void run() {
		
		try {

			Random ran = new Random();
			Message msg = null;

			sendInitialMessage();
			while(!runLongEnough()){
				long elapsedTime = ((RequestTracer)service).getElapsedTime();
				do {
					msg = service.readMessage(queueId, null, false, null);
					if (tracing) {
						elapsedTime += ((RequestTracer)service).getElapsedTime();
					}
					if (msg != null) {
						break;
					}
					else {
						elapsedTime += pollInterval;
						Thread.sleep(pollInterval);
					}

				} while (msg == null && !runLongEnough());
				
				long t1 = System.currentTimeMillis();
			    if( msg==null ){
				    System.out.println(this.ID + " time up. Terminating..");
				    service.deregister();
				    return;
			    }
		       
				Integer receiver = randomClientList.get(ran.nextInt(randomClientList.size()));
				msg.setQueueID(queueId);
		    	msg.setReceiverID(receiver);
		    	elapsedTime += (System.currentTimeMillis() - t1);
		    	long thinkTime = (waitInterval - elapsedTime);
		    	if (tracing) {
					((RequestTracer)service).getFileLog().write(System.currentTimeMillis()
							+ " thinktime:" + thinkTime + "\n");
				}
				if (thinkTime > 0) {
			    Thread.sleep(thinkTime);
			}
			if (!service.sendMessage(msg)) {
				System.out.println(this.ID + " failed to send a message. Terminating..");
				service.deregister();
				return;
				}
			    
		}
		service.deregister();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
