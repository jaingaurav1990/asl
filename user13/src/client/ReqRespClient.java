package client;

import helperClasses.Message;
import logging.RequestTracer;

import java.io.IOException;
import java.net.InetAddress;

public class ReqRespClient extends Client {
	
	protected int readQ = -1;
	protected int writeQ = -1;
	protected boolean requester = false;
	
	public ReqRespClient(InetAddress middlewareIP, int middlewarePort,
			long timeRunning, int readQ, int writeQ, boolean requester, Boolean tracing, long waitInterval, long pollInterval) throws IOException {
		super(middlewareIP, middlewarePort, timeRunning, tracing, waitInterval, pollInterval);
		this.readQ  = readQ;
		this.writeQ = writeQ;
		this.requester = requester; 
		if (tracing) {
			((RequestTracer)service).getFileLog().write("Client " + this.ID + " started with parameters " 
					+ "middlewareIP: " + middlewareIP + " middlewarePort:" + middlewarePort 
					+ " readQ: " + readQ + " writeQ: " + writeQ  
					+ " timeRunning: " + timeRunning + " tracing: " + tracing
					+ " waitInterval: " + waitInterval + " pollInterval: " + pollInterval
					+ "\n");
	
		}
	}
	
	protected void sendInitialMessage() throws IOException {
		int priority = 5; // Nothing special about 5
		Message msg = new Message(this.ID, null, writeQ, priority, Integer.toString(0));
		if (!service.sendMessage(msg)) {
			throw new RuntimeException();
		}
	}
	
	@Override
	public void run() {
		Message msg = null;
		try {
			int expected = 1;
			if(requester) {
				sendInitialMessage();
			}

			while (!runLongEnough()) {			
				long elapsedTime = ((RequestTracer)service).getElapsedTime();
				do {
					msg = service.getMessage(readQ, null, false, null);
					if (tracing) {
						elapsedTime += ((RequestTracer)service).getElapsedTime();
					}
					if (msg != null) {
						break;
					}
					else {
						elapsedTime += pollInterval;
						Thread.sleep(pollInterval);
					}
				} while (msg == null && !runLongEnough());
				
				long t1 = System.currentTimeMillis();
				if(msg == null) {
				    System.out.println(this.ID + " time up. Terminating..");
				    service.deregister();
				    return;
			    }
				Integer receivedInt = Integer.parseInt(msg.getText());
				msg.setMessage(Integer.toString(receivedInt + 1));
				if (requester) {
					if (receivedInt != expected) {
						((RequestTracer)service).getFileLog().write("Mismatch between expected and received text."
								+ " Raising runtime Exception");
						((RequestTracer)service).closeFileLog();
						throw new RuntimeException();
					}
					expected += 2;
					msg.setReceiverID(null); // Request can be served by any of the servers
				}
				else {
					/*
					 * Server needs to explicitly set the receiverID
					 * in the message so that only the original requester
					 * receives that message.
					 */
					Integer requester = msg.getSenderID();
					msg.setReceiverID(requester);
				}

				msg.setQueueID(writeQ);
				elapsedTime += System.currentTimeMillis() - t1;
				long thinkTime = (waitInterval - elapsedTime);
				if (tracing) {
					((RequestTracer)service).getFileLog().write(System.currentTimeMillis() + ": " + "thinkTime: " + thinkTime + "\n");
				}
				if (requester && thinkTime > 0) {
					/*
					 * Only the requester clients should use a 
					 * thinkTime before generating another request. 
					 * Serving clients should return as soon as 
					 * possible.
					 */
					Thread.sleep(thinkTime);
				}
				if (!service.sendMessage(msg)) {
					System.out.println(this.ID + " failed to send a message. Terminating cleanly");
					service.deregister();
					return;
				}

			}
			service.deregister();
			return;
		} catch (IOException e) {
			lgr.log(ERROR, e.getMessage(), e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
