package helperClasses;
import java.util.*;

public class Message {
	
	private Integer messageID;

	/*
	 * Unique ID identifying the sender of the 
	 * message. The sender client must always set 
	 * this field.
	 */
	private Integer senderID;
	/*
	 * Unique ID identifying the intended receiver
	 * of the message. May be NULL.
	 */
	private Integer receiverID; 
	
	/*
	 * Unique ID identifying the queue this message
	 * must be put in. At least, one of queueID OR 
	 * queueIDs (see next attribute) must be specified.
	 */
	private Integer queueID;
	
	/*
	 * Unique ID identifying the list of queues this 
	 * message must be put in atomically. At least, one 
	 * of queueID OR queueIDs (see next attribute) must
	 * be specified.
	 */
	private List<Integer> queueIDs;
	
	/*
	 * An integer indicating the priority of the message
	 */
	private Integer priority;
	
	private long timestamp; // ??
	
	/*
	 * An integer indicating the context ID of the message
	 * when two clients are involved in multiple
	 * conversations.
	 */
	private Integer context;
	
	/*
	 * A String representing the actual content of the 
	 * message.
	 */
	private String text;
	private Integer counter;
	private List<Integer> track;
		
	//One way Message
	public Message(Integer senderID, Integer receiverID, Integer queueID,
			Integer priority, String text) {
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.queueID = queueID;
		this.queueIDs = null;
		this.priority = priority;
		this.text = text;
		this.context = null;
		this.counter = 0;
		track = new ArrayList<Integer>();
	}
	
	public Message(Integer senderID, Integer receiverID, List<Integer> queueIDs,
			Integer priority, String text) {
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.queueID = null;
		this.queueIDs = queueIDs;
		this.priority = priority;
		this.text = text;
		this.context = null;
	}
	
	//request-response Message
	public Message(Integer senderID, Integer receiverID, Integer queueID,
			Integer context, Integer priority, String text){
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.queueID = queueID;
		this.context = context;
		this.priority = priority;
		this.text = text;
		this.counter = 0;
		track = new ArrayList<Integer>();
	}
	
	public Message(Integer senderID, Integer receiverID, List<Integer> queueIDs,
			Integer context, Integer priority, String text){
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.queueID = null;
		this.queueIDs = queueIDs;
		this.context = context;
		this.priority = priority;
		this.text = text;
		this.counter = 0;
		track = new ArrayList<Integer>();
	}
	
	public void setsenderID(Integer senderID){
		this.senderID = senderID;
	}
	
	public void setReceiverID(Integer receiverID){
		this.receiverID = receiverID;
	}
	
	public void setQueueID(Integer queueID){
		
		this.queueID = queueID;
		
	}
	
	public void updateTrack(Integer clientID){
		track.add(clientID);
	}
	
	public void updateCounter(){
		this.counter++;
	}
	
	public long getMessageID(){
		return this.messageID;
	}
	
	public Integer getSenderID(){
		return this.senderID;
	}
	
	public Integer getReceiverID(){
		return this.receiverID;
	}
	
	public Integer getQueueID(){
		return this.queueID;
	}
	
	public List<Integer> getQueueIDs() {
		return this.queueIDs;
	}
	
	public void setQueueIDs(List<Integer> queueIDs) {
		this.queueIDs = queueIDs;
	}

	public Integer getPriority(){
		return this.priority;
	}
	
	public void setTimestamp(long ts) {
		this.timestamp = ts;
	}
	
	public long getTimestamp(){
		return this.timestamp;
	}
	
	public Integer getContext(){
		return this.context;
	}
	
	public int getCount() {
		return this.counter;
	}
	
	public String getText(){
		return this.text;
	}
	
	public void setMessage(String text) {
		this.text = text;
	}

}
