package db;

import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

public class DataBaseEndPoint {
	private Connection con = null;
	private PreparedStatement register = null;
	private PreparedStatement deregister = null;
	private PreparedStatement createQueue = null;
	private PreparedStatement deleteQueue = null;
	private PreparedStatement findRequest = null;
	private PreparedStatement sendOne = null;
	private PreparedStatement sendMany = null;
	private PreparedStatement readOrGetMessage = null;
	private PreparedStatement listQueues = null;
	private PreparedStatement listQueuesMsgs = null;
	private PreparedStatement listUsers = null;
	private PreparedStatement deleteMessage = null;
	
	public DataBaseEndPoint(Connection con) throws SQLException {
		this.con = con;
		this.register = con.prepareStatement("SELECT createClient()");
		this.deregister = con.prepareStatement("SELECT deleteClient(?)");
		this.createQueue = con.prepareStatement("SELECT createQueue()");
		this.deleteQueue = con.prepareStatement("SELECT deleteQueue(?)");
		this.findRequest = con.prepareStatement(
			"SELECT * FROM message WHERE context > 0 LIMIT 1");
		this.sendOne = con.prepareStatement(
			"SELECT insertMessageOne(?, ?, ?, ?, ?, ?)");
		this.sendMany = con.prepareStatement("SELECT insertMessageMany(?, ?, ?, ?, ?, ?)");
		this.readOrGetMessage = con.prepareStatement("SELECT * from readOrGetMessage(?,?,?,?,?,?)");

		this.listQueues = con.prepareStatement("SELECT listQueue()");
		this.listQueuesMsgs = con.prepareStatement("SELECT listQueues(?)");
		this.listUsers = con.prepareStatement("SELECT listUsers()");
		this.deleteMessage = con.prepareStatement("SELECT deleteMessage(?)");
	}
	
	public ResultSet registerF() throws SQLException {
		return register.executeQuery();
	}
	
	public ResultSet deregisterF(int clientId) throws SQLException {
		deregister.setInt(1, clientId);
		return deregister.executeQuery();
	}
	
	public ResultSet createQueueF() throws SQLException {
		return createQueue.executeQuery();
	}
	
	public ResultSet deleteQueueF(int queueId) throws SQLException {
		deleteQueue.setInt(1, queueId);
		return deleteQueue.executeQuery();
	}
	
	public ResultSet sendMessageF(int senderId, int receiverId, int queueId,
			int context, int priority, String message, BufferedWriter fileLog) throws SQLException, IOException {
		sendOne.setInt(1, senderId);
		if(receiverId > -1)
			sendOne.setInt(2, receiverId);
		else
			sendOne.setNull(2, Types.NULL);
		sendOne.setInt(3, queueId);
		if(context > -1)
			sendOne.setInt(4, context);
		else
			sendOne.setNull(4, Types.NULL);
		sendOne.setInt(5, priority);
		sendOne.setString(6, message);
		long start = System.nanoTime();
		ResultSet rs = sendOne.executeQuery();
		long end = System.nanoTime();
		long elapsed = (end - start)/1000000;
		fileLog.write(System.currentTimeMillis() + ": " + "sendOne.executeQuery(): " + elapsed + "\n");
		
		return rs;
	}
	
	public ResultSet sendMessageF(int senderId, int receiverId, List<Integer> queueIds,
			int context, int priority, String message, BufferedWriter fileLog) throws SQLException, IOException {
	
		sendMany.setInt(1, senderId);
		if(receiverId > -1)
			sendMany.setInt(2, receiverId);
		else
			sendMany.setNull(2, Types.NULL);
		sendMany.setArray(3, con.createArrayOf("integer", queueIds.toArray()));
		if(context > -1)
			sendMany.setInt(4, context);
		else
			sendMany.setNull(4, Types.NULL);
		sendMany.setInt(5, priority);
		sendMany.setString(6, message);
		
		long start = System.nanoTime();
		ResultSet rs = sendMany.executeQuery();
		long end = System.nanoTime();
		long elapsed = (end - start)/1000000;
		fileLog.write(System.currentTimeMillis() + ": " + "sendMany.executeQuery(): " + elapsed + "\n");
		
		return rs;
	}
	
	private void setIdorNull(PreparedStatement pst, int parameterIndex,
			Integer val) throws SQLException {
		if (val == null) {
			pst.setNull(parameterIndex, Types.INTEGER);
		}
		else {
			pst.setInt(parameterIndex, val);
		}
		
	}

	public ResultSet readOrGetMessage(Integer receiverId, Integer qid, Integer context, 
			Integer senderId, boolean byTimeStamp, boolean remove, BufferedWriter fileLog) throws SQLException, IOException {

		readOrGetMessage.setInt(1, receiverId);
		setIdorNull(readOrGetMessage, 2, qid);
		setIdorNull(readOrGetMessage, 3, context);
		setIdorNull(readOrGetMessage, 4, senderId);
		readOrGetMessage.setBoolean(5, byTimeStamp);
		if (remove) {
			readOrGetMessage.setBoolean(6, true);
		}
		else {
			readOrGetMessage.setBoolean(6, false);
		}

		long start = System.nanoTime();
		ResultSet rs = readOrGetMessage.executeQuery();
		long end = System.nanoTime();
		long elapsed = (end - start)/1000000;
		fileLog.write(System.currentTimeMillis() + ": " + "readOrGetMessage.executeQuery(): " + elapsed + "\n");
		return rs;
	}
	
	public ResultSet findReqRespMessage() throws SQLException {
		return findRequest.executeQuery();
	}
	
	public ResultSet listQueuesF() throws SQLException {
		return listQueues.executeQuery();
	}
	
	public ResultSet listQueuesMsgsF(int receiverId) throws SQLException {
		listQueuesMsgs.setInt(1, receiverId);
		return listQueuesMsgs.executeQuery();
	}
	
	public ResultSet listUsersF() throws SQLException {
		return listUsers.executeQuery();
	}
	
	public ResultSet deleteMessageF(int messageId) throws SQLException {
		deleteMessage.setInt(1, messageId);
		return deleteMessage.executeQuery();
	}
	
	public final void close() throws SQLException {
		this.createQueue.close();
		this.deleteQueue.close();
		this.findRequest.close();
		this.listQueues.close();
		this.listQueuesMsgs.close();
		this.listUsers.close();
		this.register.close();
		this.deregister.close();
		this.sendMany.close();
		this.sendOne.close();
		this.deleteMessage.close();
		this.readOrGetMessage.close();
		this.con.close();
	}
}
