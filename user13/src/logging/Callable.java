package logging;
/*
 * A method that returns a value.
 * Follows the command pattern.
 * 
 * @return Return the return value of wrapped method
 */
public interface Callable<T> {
	T execute();
}
