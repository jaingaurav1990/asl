package logging;

import helperClasses.Message;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import service.MessagingService;

public class RequestTracer implements MessagingService {

	private final MessagingService service;
	private long elapsedTime;
	private BufferedWriter fileLog; // It is the responsibility of the caller to set fileLog
	
	public RequestTracer(MessagingService service) {
		if (service == null) {
			throw new NullPointerException();
		}
		this.service = service;
		this.elapsedTime = 0;
	}
	
	public long getElapsedTime() {
		return elapsedTime;
	}
	/*
	 * Sets the file to be used as a log by RequestTracer.
	 * It is the responsibility of the invoker to set the
	 * fileLog properly before calling any methods on
	 * RequestTracer
	 * 
	 * @param fileLog File to be used as a log
	 */
	public void setFileLog(BufferedWriter fileLog) {
		this.fileLog = fileLog;
	}

	public BufferedWriter getFileLog() {
		return fileLog;
	}
	/*
	 * Flushes and closes the file Log. 
	 * 
	 * The invoker must call this method to flush all
	 * log records on file on the disk.
	 */
	public void closeFileLog() {
		try {
			this.fileLog.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Integer register() {
		return runAndMeasure(new Callable<Integer>() {
			@Override
			public Integer execute() {
				return service.register();
			}
		});
	}

	@Override
	public void deregister() {
		runAndMeasure(new Callable<Void>() {
			@Override
			public Void execute() {
				service.deregister();
				return null;
			}
		});
		closeFileLog();
	}

	@Override
	public Integer createQueue() {
		return runAndMeasure(new Callable<Integer>() {
			@Override
			public Integer execute() {
				return service.createQueue();
			}
		});
	}

	@Override
	public Boolean deleteQueue(final int queueId) {
		return runAndMeasure(new Callable<Boolean>() {
			@Override
			public Boolean execute() {
				return service.deleteQueue(queueId);
			}
		});
	}

	@Override
	public Boolean sendMessage(final Message msg) {
		return runAndMeasure(new Callable<Boolean>() {
			@Override
			public Boolean execute() {
				return service.sendMessage(msg);
			}
		});

	}

	@Override
	public Message readMessage(final Integer queueID, final Integer context,
			final Boolean byTimeStamp, final Integer senderOfMessage) {
		return runAndMeasure(new Callable<Message>() {
			@Override
			public Message execute() {
				return service.readMessage(queueID, context, byTimeStamp, senderOfMessage);
			}
		});
	}

	@Override
	public Message getMessage(final Integer queueID, final Integer context,
			final Boolean byTimeStamp, final Integer senderId) {
		return runAndMeasure(new Callable<Message>() {
			@Override
			public Message execute() {
				return service.getMessage(queueID, context, byTimeStamp, senderId);
			}
		});
	}

	@Override
	public List<Integer> listClients() {
		return runAndMeasure(new Callable< List<Integer> >() {
			@Override
			public List<Integer> execute() {
				return service.listClients();
			}
		});
	}

	@Override
	public List<Integer> listQueues(final int receiverID) {
		return runAndMeasure(new Callable< List<Integer> >() {
			@Override
			public List<Integer> execute() {
				return service.listQueues(receiverID);
			}
		});
	}

	@Override
	public List<Integer> listQueues() {
		return runAndMeasure(new Callable< List<Integer> >() {
			@Override
			public List<Integer> execute() {
				return service.listQueues();
			}
		});
	}

	private <T> T runAndMeasure(Callable<T> function) {
		if (function == null) {
			throw new NullPointerException();
		}
		
		Exception exception = null;
		elapsedTime = System.nanoTime();
		try {
			return function.execute();
		}
		catch (RuntimeException e) {
			exception = e;
			throw e;
		}
		catch (Exception e) {
			/*
			 *  We are seeing a checked exception being thrown by one
			 *  of the messaging service methods, which should not be 
			 *  happening since none of the methods throw checked
			 *  exceptions (Change the logic here if that's not the 
			 *  case). Wrap the checked exception and throw it again
			 *  as an unchecked exception
			 *.
			 */
			exception = e;
			throw new RuntimeException(e);
		}
		finally {
			elapsedTime = (System.nanoTime() - elapsedTime)/1000000; // Log time in milliseconds
			String name = function.getClass().getName();
			if (exception == null) {
				try {
					this.fileLog.write(System.currentTimeMillis() + ": " + name + ": " + elapsedTime + "\n");
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			else {
				try {
					this.fileLog.write(System.currentTimeMillis() + ": " + "EXCEPTION in " + name + ": " + elapsedTime + "\n");
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
		}
		}
	}
}
