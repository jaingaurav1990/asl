package middleware;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ServerShutdown {
	
	private Socket middlewareSocket;
	private PrintWriter out;
	private BufferedReader in;
	public ServerShutdown(InetAddress middlewareIP, int middlewarePort) throws IOException{
		
		this.middlewareSocket = new Socket(middlewareIP, middlewarePort);
		this.out = new PrintWriter(this.middlewareSocket.getOutputStream(),true);
		this.in = new BufferedReader(new InputStreamReader(this.middlewareSocket.getInputStream()));
	}
	
	public static void main(String args[]){
		
	String middlewareHost = args[0];
	int middlewarePort = Integer.parseInt(args[1]);
		
	//String middlewareHost = "localhost";
	//int middlewarePort = 3456;	
		
		
	InetAddress middlewareIp;
	try {
		middlewareIp = InetAddress.getByName(middlewareHost);
		ServerShutdown serverShutdown = new ServerShutdown(middlewareIp, middlewarePort);
		serverShutdown.out.println("shutdown");
		String status = serverShutdown.in.readLine();
		if (!helperClasses.Constants.OK.equals(status)) {
			System.out.println("Server Shutting down");
		} 
		else {
			System.out.println("Server not Shutting down");
		}
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	
	

}
