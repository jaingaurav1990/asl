package middleware;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;

import org.postgresql.ds.PGSimpleDataSource;

import db.DataBaseEndPoint;
import helperClasses.Constants;
import helperClasses.Convertor;
import helperClasses.Message;

public class Server implements Runnable, Constants {

	private Socket socket = null;
	private boolean running = true;
	private ServerMessagingService service;
	private static final String logDir = System.getProperty("java.io.tmpdir");
	private BufferedWriter fileLog;
	Logger lgr = null;

	public Server(Socket socket, ArrayBlockingQueue<DataBaseEndPoint> queries, long connectionNum) throws IOException {
		this.socket = socket;
		
		this.fileLog = new BufferedWriter(new FileWriter(new File(logDir + File.separator + "serverThread" + connectionNum)));
		this.service = new ServerMessagingService(queries, this.fileLog);
		lgr = Logger.getLogger(Server.class.getName());
	}


	public static void main(String[] args) {
		if (args.length < 5) {
			System.out.println("Usage: java Server.java <dbHost> <dbPort> <dbUser> <dbNumConnections> <expectedConnections>");
			System.exit(-1);
		}

		String dbHost = args[0];
		Integer dbPort = Integer.parseInt(args[1]);
		String dbUser = args[2];
		int numConnections = Integer.parseInt(args[3]);
		int expectedConnections = Integer.parseInt(args[4]);
		try {

			ServerSocket serverSocket = null;
			PGSimpleDataSource dbDataSource = null;
			long connectionsAccepted = 0;
			final Logger mainLogger = Logger.getLogger("MainLogger");
			
			serverSocket = new ServerSocket(PORT);
			/*
			 * Create a DB connection pool by creating connections
			 * from a PGSimpleDataSource and putting them in a
			 * ArrayBlockingQueue. Another alternative is to use
			 * PGPoolingDataSource.
			 */
			dbDataSource = new PGSimpleDataSource();
			dbDataSource.setServerName(dbHost);
			dbDataSource.setPortNumber(dbPort);
			dbDataSource.setDatabaseName(db_name);
			dbDataSource.setUser(dbUser);
			dbDataSource.setTcpKeepAlive(true);

			ArrayBlockingQueue<DataBaseEndPoint> connectionPool = new ArrayBlockingQueue<DataBaseEndPoint>(numConnections);
			for (int i = 0; i < numConnections; i++) {
				DataBaseEndPoint dbep = new DataBaseEndPoint(dbDataSource.getConnection());
				connectionPool.offer(dbep);
			}
		
			System.out.println("Server listening on port: " + serverSocket.getLocalPort());
			ArrayList<Thread> threads = new ArrayList<Thread>();
			while (connectionsAccepted < expectedConnections) {
	
				Socket clientSocket = serverSocket.accept();
				connectionsAccepted++;
				mainLogger.log(STAT_MSG, "Accepted connection from IP: "
						+ clientSocket.getInetAddress());
				Thread t = new Thread(new Server(clientSocket, connectionPool, connectionsAccepted));
				threads.add(t);
				t.start();
			}

			for (Thread t:threads) {
				t.join();
			}
			
			System.out.println("All child threads finished cleanly. Clean up database connection pool");
			mainLogger.log(STAT_MSG, "Accepted a total of " + connectionsAccepted
				+ " connections");
			for (Iterator<DataBaseEndPoint> iterator = connectionPool.iterator(); iterator.hasNext();) {
				DataBaseEndPoint dataBaseEndPoint = (DataBaseEndPoint) iterator.next();
				dataBaseEndPoint.close();
			}
			serverSocket.close();
			System.out.println("Done.");
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			/*
			 * Setting boolean autoflush to false in the Printwriter
			 * constructor means that out.println won't automatically 
			 * flush. Take care to call out.flush after out.println 
			 */
			out = new PrintWriter(socket.getOutputStream(), false);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String cmd, message_to_client;

			while (running) {
				long start = System.nanoTime();
				cmd = in.readLine();
				long end = System.nanoTime();
				long elapsed = (end - start)/1000000;
				this.fileLog.write(System.currentTimeMillis() + ": " + "handleCmd." + cmd + ".readline(): " + elapsed + "\n");
				
				if (cmd != null) { 
					start = System.nanoTime();
					this.fileLog.write(System.currentTimeMillis() + ": " + "handleCmd()\n");
					message_to_client = handleCmd(cmd, in);
					end = System.nanoTime();
					elapsed = (end - start)/1000000;
					this.fileLog.write(System.currentTimeMillis() + ": " + "handleCmd." + cmd + "(): " + elapsed + "\n");
					
					long start2 = System.nanoTime();
					if (message_to_client != null) {
						if (message_to_client.contains("!")) {
							String[] mtc = message_to_client.split("!");
							out.println(mtc[0]);
							out.println(mtc[1]);
							out.flush();
						} else {
							out.println(message_to_client);
							out.flush();
						}
					}
					
					long stop = System.nanoTime();
					long elapsed2 = (stop - start2)/1000000;
					this.fileLog.write(System.currentTimeMillis() + ": " + "message_to_client()" + ": " + elapsed2 + "\n");
					elapsed = (stop - start)/1000000; // Convert to milliseconds
					/*
					 * Following line represents the most coarse logging at the Server.
					 * It logs the total time taken since the command from client was read
					 * and it was executed at the Server and response sent back.
					 */
					this.fileLog.write(System.currentTimeMillis() + ": " + cmd + ": " + elapsed + "\n");
				}
				else {
					/*
					 * Use the fact that readLine would return null if
					 * the other side closed the connection. So, we can
					 * safely assume that the client on the other side has
					 * closed the connection and the corresponding server
					 * thread (this one) can safely exit too.
					 */
					running = false;
				}
			}
			in.close();
			out.close();
			socket.close();
			this.fileLog.close();
			lgr.info("Server thread gracefully terminating");
		} catch (IOException e) {
			lgr.log(ERROR, "IO Error when reading from socket.");
			throw new RuntimeException(e);
		}
	}

	private String handleCmd(String cmd, BufferedReader in) throws IOException {
		String message_to_client = null, inline;
		if (cmd.equals("register")) {
			int id = service.register();
			message_to_client = id >= 0 ? OK + "!" + "id:" + id : "Registration failed.";
		} else if (cmd.equals("deregister")) {
			service.deregister();
			running = false;
			message_to_client = OK ; // An exception would have been already thrown if this operation didn't succeed
		} else if(cmd.equals("users")) {
			List<Integer> users = service.listClients();
			message_to_client = users == null ? "Failure." : OK + "!users:" + users;
		} else if (cmd.equals("create")) {
			int id = service.createQueue();
			message_to_client = id >= 0 ? OK + "!" + "id:" + id : "Failure";
		} else {
			long start = System.nanoTime();
			inline = in.readLine();
			long end = System.nanoTime();
			long elapsed = (end - start)/1000000;
			this.fileLog.write(System.currentTimeMillis() + ": " + "handleCmd." + cmd + ".in.readLine(): " + elapsed + "\n");
			if (inline == null) {
				throw new NullPointerException();
		    }

			start = System.nanoTime();
			HashMap<String, String> query = Convertor.stringToMap(inline);
			end = System.nanoTime();
			elapsed = (end - start)/1000000;
			this.fileLog.write(System.currentTimeMillis() + ": " + "handleCmd." + cmd + ".Convertor.stringToMap(): " + elapsed + "\n");

			if (cmd.equals("delete")) {
				boolean success = deleteQueueFromString(query);
				message_to_client = success ? OK : "Faulty message";
			} else if (cmd.equals("send")) {
				start = System.nanoTime();
				boolean success = sendMessageFromString(query);
				end = System.nanoTime();
				elapsed = (end - start)/1000000;
				this.fileLog.write(System.currentTimeMillis() + ": " + "send.sendMessageFromString(): " + elapsed + "\n");
				message_to_client = success ? OK : "Send to DB Failed";
			} else if (cmd.equals("get") || cmd.equals("read")) {
				start = System.nanoTime();
				String str = messageFromString(cmd, query);
				end = System.nanoTime();
				elapsed = (end - start)/1000000;
				this.fileLog.write(System.currentTimeMillis() + ": " + "readOrGet.messageFromString(): " + elapsed + "\n");
				message_to_client = str != null ? OK + "!" + str : "Failure message";
			} else if (cmd.equals("list")) {
				message_to_client = OK + "!" + listQueueFromString(query);
			} else {
				message_to_client = "Bad Request";
			}
		}
		return message_to_client;
	}

	private boolean deleteQueueFromString(HashMap<String, String> query) {
		if (query.containsKey("id")) {
			return service.deleteQueue(Integer.valueOf(query.get("id")));
		}
		return false;
	}

	private boolean sendMessageFromString(HashMap<String, String> query) throws IOException {
		
		Integer receiver = -1,sender = -1,context = -1,priority = 5,queue = 1;
	
		String message = query.get("message");
		if(query.get("receiver") != null)
			receiver = Integer.valueOf(query.get("receiver"));
		if(query.get("sender") != null)
			sender = Integer.valueOf(query.get("sender"));
		if(query.get("context") != null)
			context = Integer.valueOf(query.get("context"));
		if(query.get("priority") != null)
			priority = Integer.valueOf(query.get("priority"));
		
		if(query.get("queue") != null){
		    if( query.get("queue").contains("[")){
				List<Integer> queues = Convertor.stringToList(query.get("queue"));
				return service.sendMessage(new Message(sender, receiver, queues, context, priority, message));
		    } else{
			    queue = Integer.valueOf(query.get("queue"));
			    return service.sendMessage(new Message(sender, receiver, queue, context, priority, message));
		    }
		}
		
		long start = System.nanoTime();
		boolean result = service.sendMessage(new Message(sender, receiver, queue, context, priority, message));
		long end = System.nanoTime();
		long elapsed = (end - start)/1000000;
		this.fileLog.write(System.currentTimeMillis() + ": " + "sendMessageFromString.sendMessage(): " + elapsed + "\n");
		return result;
	}

	private String messageFromString(String cmd, HashMap<String, String> query) throws IOException {
		Integer queue, context, sender;
		boolean arrive_time_order = false;
		
		if (query.containsKey("queue") && query.containsKey("context") && !query.containsKey("sender"))
			return "wrong query";
		if (!query.containsKey("queue") && query.containsKey("context") && !query.containsKey("sender"))
		    return "wrong query";
		
		if(query.containsKey("queue"))
			queue = Integer.valueOf(query.get("queue"));
		else
			queue = null;
		
		if(query.containsKey("context"))
			context = Integer.valueOf(query.get("context"));
		else
			context = null;
		
		if(query.containsKey("sender"))
			sender = Integer.valueOf(query.get("sender"));
		else
			sender = null;
		
		if (query.containsKey("arrive_time_order")) {
			arrive_time_order = Boolean.parseBoolean(query
					.get("arrive_time_order"));
		}
		
		Boolean remove = true;
		if (("read").equals(cmd)) {
			remove = false;
		}
		else {
			remove = true;
		}

		long start = System.nanoTime();
		String result = service.readOrGetMessage(queue, context, sender, arrive_time_order, remove);
		long end = System.nanoTime();
		long elapsed = (end - start)/1000000;
		this.fileLog.write(System.currentTimeMillis() + ": " + "messageFromString.readOrGetMessage(): " + elapsed + "\n");
		
		return result;
	}

	private String listQueueFromString(HashMap<String, String> query) {
		Integer receiver = null;
		if( query.containsKey("receiver") && query.get("receiver") != null)
			receiver = Integer.valueOf(query.get("receiver"));
		
		List<Integer> ids;
		if (receiver != -1)
			ids = service.listQueues(receiver);
		else
			ids = service.listQueues();

		String str = "queues:" + ids;
		return str;
	}


}
