package middleware;

import helperClasses.Convertor;
import helperClasses.Message;

import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import db.DataBaseEndPoint;
import service.MessagingService;

public class ServerMessagingService implements MessagingService {

	private ArrayBlockingQueue<DataBaseEndPoint> connectionPool;
	private int clientId;
	private BufferedWriter fileLog;
	
	public ServerMessagingService(ArrayBlockingQueue<DataBaseEndPoint> connectionPool, BufferedWriter fileLog) {
		this.connectionPool = connectionPool;
		this.fileLog = fileLog;
	}
	
	@Override
	public Integer register() {
		int clientId= -1;
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.registerF();
			if (rs.next()) {
				clientId = rs.getInt(1);
			}
			rs.close();
			connectionPool.put(dbep);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		this.clientId = clientId;
		return clientId;
	}

	@Override
	public void deregister() {
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.deregisterF(this.clientId);
			
			if (rs.next() && clientId != rs.getInt(1)) {
				throw new RuntimeException();
			}
			rs.close();
			connectionPool.put(dbep);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Integer createQueue() {
		Integer queueId = -1;
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.createQueueF();
			if (rs.next()) {
				queueId = rs.getInt(1);
			}
			rs.close();
			connectionPool.put(dbep);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		return queueId;
	}

	@Override
	public Boolean deleteQueue(int queueId) {
	
		System.out.println("queueid=" + queueId);
		boolean success = false;
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.deleteQueueF(queueId);
			if (rs.next()) {
				success = true;
			}
			rs.close();
			connectionPool.put(dbep);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		return success;
	}

	@Override
	public Boolean sendMessage(Message msg) {
		Integer senderId = msg.getSenderID();
		Integer receiverId = msg.getReceiverID();
		Integer queueId = msg.getQueueID();
		List <Integer> queueIds = msg.getQueueIDs();
		Integer context = msg.getContext();
		Integer priority = msg.getPriority();
		String text = msg.getText();
		boolean success = false;
		ResultSet rs = null;
		try {
			long start = System.nanoTime();
			DataBaseEndPoint dbep = connectionPool.take();
			long end = System.nanoTime();
			long elapsed = (end - start)/1000000;
			this.fileLog.write(System.currentTimeMillis() + ": " + "sendMessage.connectionPool.take(): " + elapsed + "\n");
			if (queueId != null) {
				rs = dbep.sendMessageF(senderId, receiverId, queueId, context, priority, text, this.fileLog);
			}
			else {
				rs = dbep.sendMessageF(senderId, receiverId, queueIds, context, priority, text, this.fileLog);
			}
			if (rs != null && rs.next()) {
				success = rs.getBoolean(1);
			}
			rs.close();
			connectionPool.put(dbep);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
		return success;
	}

	@Override
	public Message readMessage(Integer queueId, Integer context,
			Boolean byTimeStamp, Integer senderOfMessage) {
		String str = readOrGetMessage(queueId, context, senderOfMessage, byTimeStamp, false);
		HashMap<String, String> query = Convertor
				.stringToMap(str);
		return Convertor.mapToMessage(query);
	}

	@Override
	public Message getMessage(Integer queueID, Integer context,
			Boolean byTimeStamp, Integer senderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> listClients() {
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.listUsersF();
			List<Integer> q_list = new ArrayList<Integer>();
			while (rs.next()) {
				q_list.add(rs.getInt(1));
			}
			rs.close();
			connectionPool.put(dbep);
			return q_list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Integer> listQueues(int receiverId) {
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.listQueuesMsgsF(receiverId);
			List<Integer> q_list = new ArrayList<Integer>();
			while (rs.next()) {
				q_list.add(rs.getInt(1));
			}
			rs.close();
			connectionPool.put(dbep);
			return q_list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Integer> listQueues() {
		try {
			DataBaseEndPoint dbep = connectionPool.take();
			ResultSet rs = dbep.listQueuesF();
			List<Integer> q_list = new ArrayList<Integer>();
			while (rs.next()) {
				q_list.add(rs.getInt(1));
			}
			rs.close();
			connectionPool.put(dbep);
			return q_list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public String readOrGetMessage(Integer qid, Integer context, Integer senderId, 
			Boolean byTimeStamp, Boolean remove) {
		String msg = null;
		ResultSet rs = null;
		try {
			long start = System.nanoTime();
			DataBaseEndPoint dbep = connectionPool.take();
			long end = System.nanoTime();
			long elapsed = (end - start)/1000000;
			this.fileLog.write(System.currentTimeMillis() + ": " + "readOrGetMessage.connectionPool.take(): " + elapsed + "\n");

			rs = dbep.readOrGetMessage(this.clientId, qid, context, senderId, byTimeStamp, remove, this.fileLog);
			if (rs != null && rs.next()) {
				start = System.nanoTime();
				msg = Convertor.ResultMessageToString(rs);
				end = System.nanoTime();
				elapsed = (end - start)/1000000;
				this.fileLog.write(System.currentTimeMillis() + ": " + "readOrGetMessage.Convertor.ResultMessageToString(): " + elapsed + "\n");
			}
			rs.close();
			connectionPool.put(dbep);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return msg;
	}
}
