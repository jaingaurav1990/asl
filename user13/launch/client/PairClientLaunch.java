package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class PairClientLaunch {
	
private static ArrayList<Thread> clientListSender = new ArrayList<Thread>();
private static ArrayList<Thread> clientListReceiver = new ArrayList<Thread>();
	
	public static void main(String args[]) throws InterruptedException, IOException {
		
		if (args.length < 8) {
			System.out.println("Usage: java ReqRespClientLaunch <middlewareHost> <middlewarePort> "
						+ " <numClientPairs> <timeRunning (in seconds> <waitInterval (in ms)>" 
						+ "<pollInterval (in ms)> <tracing (true or false)> <startId>");
				System.exit(-1);
		}
		
	    String middlewareHost = args[0];
	    int middlewarePort = Integer.parseInt(args[1]);
		int numClientPairs = Integer.parseInt(args[2]);
		long timeRunning = Long.parseLong(args[3]) * 1000;
		long waitInterval = Long.parseLong(args[4]);
		long pollInterval = Long.parseLong(args[5]);
		Boolean tracing = Boolean.parseBoolean(args[6]);
		int startId = Integer.parseInt(args[7]);
		
        InetAddress middlewareIp = InetAddress.getByName(middlewareHost);
		
	    Socket middlewareSocket = new Socket(middlewareIp, middlewarePort);
		PrintWriter out = new PrintWriter(middlewareSocket.getOutputStream(),
					true);
		BufferedReader in = new BufferedReader(new InputStreamReader(
					middlewareSocket.getInputStream()));
		MessagingServiceImpl service = new MessagingServiceImpl(out, in, middlewareSocket);
		int readQ = service.createQueue();
		int writeQ = service.createQueue();
	
		for(int i = 1; i <= numClientPairs; i++){
			clientListSender.add(new Thread(new PairClient(middlewareIp, middlewarePort, timeRunning, readQ, 
					writeQ, i + numClientPairs + startId - 1, true, 
					tracing, waitInterval, pollInterval)));
		}
		
		for(int i = 1; i <= numClientPairs; i++){
			clientListReceiver.add(new Thread(new PairClient(middlewareIp, middlewarePort, timeRunning, writeQ, readQ, 
					startId + i - 1, false,
					tracing, waitInterval, pollInterval)));
		}
		
		for (Thread thread : clientListSender) {
			thread.start();
		}
		
		for (Thread thread : clientListReceiver) {
			thread.start();
		}
		
		System.out.println("PairClientLauncher finished launching clients");
		for (Thread thread : clientListSender) {
			thread.join();
		}
		
		for (Thread thread : clientListReceiver) {
			thread.join();
		}
        service.deleteQueue(writeQ);
        service.deleteQueue(readQ);
		/*
		 * Clean up resources used to talk to Server
		 */
		in.close();
		out.close();
		middlewareSocket.close();
		System.out.println("Laucher terminated gracefully.");
	}	
}
