package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class RandomClientLaunch {
	
	private static ArrayList<Thread> clientList = new ArrayList<Thread>();
	
	public static void main(String args[]) throws InterruptedException, IOException {
		
		if (args.length < 9) {
			System.out.println("Usage: java RandomClientLaunch <middlewareHost> <middlewarePort> "
					+ " <numClient> <timeRunning (in seconds)> <tracing> <waitInterval (in ms)> <pollInterval (in ms)> <startId> <msgLen>");
			System.exit(-1);
		}
		
		String middlewareHost = args[0];
		int middlewarePort = Integer.parseInt(args[1]);
		int numClient = Integer.parseInt(args[2]);
		long timeRunning = Long.parseLong(args[3]) * 1000;
		Boolean tracing = Boolean.parseBoolean(args[4]);
		long waitInterval = Integer.parseInt(args[5]);
		long pollInterval = Integer.parseInt(args[6]);
		int startId = Integer.parseInt(args[7]);
		int msgLen = Integer.parseInt(args[8]);
        InetAddress middlewareIp = InetAddress.getByName(middlewareHost);
		
        Socket middlewareSocket = new Socket(middlewareIp, middlewarePort);
		PrintWriter out = new PrintWriter(middlewareSocket.getOutputStream(),
				true);
		BufferedReader in = new BufferedReader(new InputStreamReader(
				middlewareSocket.getInputStream()));
		MessagingServiceImpl service = new MessagingServiceImpl(out, in, middlewareSocket);
		int queueId = service.createQueue();
		for(int i = 0; i < numClient; i++){
			clientList.add(new Thread(new RandomClient(middlewareIp, middlewarePort, queueId, timeRunning, 
					tracing, waitInterval, pollInterval, startId, startId + numClient - 1, msgLen)));
		}
		
		for (Thread thread : clientList) {
			thread.start();
		}
		
		System.out.println("RandomClientLauncher finished launching clients");
		for (Thread thread : clientList) {
			thread.join();
		}
		
		service.deleteQueue(queueId);
		
		/*
		 * Clean up resources used to talk to Server
		 */
		in.close();
		out.close();
		middlewareSocket.close();
		System.out.println("Laucher terminated gracefully.");
		
	}

}
