package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import client.MessagingServiceImpl;

public class ReqRespClientLaunch {

	private static ArrayList<Thread> clientList = new ArrayList<Thread>();

	public static void main (String args[]) {
		if (args.length < 8) {
			System.out.println("Usage: java ReqRespClientLaunch <middlewareHost> <middlewarePort> "
					+ " <numRequester> <numServers> <timeRunning (in seconds)> <waitInterval (in milli-seconds>"
					+ "pollInterval (in milli-seconds)" + " tracing (true or false)");
			System.exit(-1);
		}

		try {

			InetAddress middlewareHost = InetAddress.getByName(args[0]);
			Integer middlewarePort = Integer.parseInt(args[1]);
			Integer numRequester = Integer.parseInt(args[2]);
			Integer numServers = Integer.parseInt(args[3]);
			Integer timeRunning = Integer.parseInt(args[4]) * 1000;
			long waitInterval = Integer.parseInt(args[5]);
			long pollInterval = Integer.parseInt(args[6]);
			Boolean tracing = Boolean.parseBoolean(args[7]);
			/*
			 * Following is a socket opened for purely administration 
			 * purposes.
			 */
			Socket middlewareSocket = new Socket(middlewareHost, middlewarePort);
			PrintWriter out = new PrintWriter(middlewareSocket.getOutputStream(),
					true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					middlewareSocket.getInputStream()));
			MessagingServiceImpl service = new MessagingServiceImpl(out, in, middlewareSocket);

			int writeQ = service.createQueue();
			int readQ = service.createQueue();
			for (int i = 0; i < numRequester; i++) {
				clientList.add(new Thread(new ReqRespClient(middlewareHost, middlewarePort, timeRunning,
						readQ, writeQ, true, tracing, waitInterval, pollInterval)));
			}
			for (int i = 0; i < numServers; i++) {
				clientList.add(new Thread(new ReqRespClient(middlewareHost, middlewarePort, timeRunning, 
						writeQ, readQ, false, tracing, waitInterval, pollInterval)));
			}
			
			for (Thread thread : clientList) {
				thread.start();
			}

			System.out.println("ReqRespClientLauncher finished launching clients");
			for (Thread thread : clientList) {
				thread.join();
			}
			
			service.deleteQueue(writeQ);
			service.deleteQueue(readQ);
	
			/*
			 * Clean up resources used to talk to Server
			 */
			in.close();
			out.close();
			middlewareSocket.close();
			System.out.println("Laucher terminated gracefully.");
		}
		catch (UnknownHostException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
