# Read the scenario description from the file scenario.txt

# Check the starting time (seconds since 1970 epoch)
startTime=`date +%s`
finishTime=`expr $startTime + 14280`
USER="user13"

sed 1d ~/scripts/scenario.txt | while read line
do
    echo $line
    # Columns are as follows
    # experimentId dbMachine dbPort serverMachine clientMachine waitInterval pollInterval messageSize noRandomClients noPairClients noReqClients noRepClients clientRunTime
    experimentId=`echo $line | awk '{print $1}'`
    dbMachine=`echo $line | awk '{print $2}'`
    dbPort=`echo $line | awk '{print $3}'`
    serverMachine=`echo $line | awk '{print $4}'` 
    clientMachine=`echo $line | awk '{print $5}'` 
    waitInterval=`echo $line | awk '{print $6}'` 
    pollInterval=`echo $line | awk '{print $7}'` 
    poolSize=`echo $line | awk '{print $8}'`
    messageSize=`echo $line | awk '{print $9}'` 
    noRandomClients=`echo $line | awk '{print $10}'`  
    clientRunTime=`echo $line | awk '{print $11}'` 
    noPairClients=0
    noReqClients=0
    noRepClients=0
    #noPairClients=`echo $line | awk '{print $11}'` 
    #noReqClients=`echo $line | awk '{print $12}'`  
    #noRepClients=`echo $line | awk '{print $13}'` 
    
    # Start the new experiment only if sufficient time left
    currentTime=`date +%s`
    timeLeft=`expr $finishTime - $currentTime`
    timeRequired=`expr $clientRunTime + 30`
    if [ $timeLeft -lt $timeRequired ]
    then
        echo "Not enough time to run another experiment. Terminating..."
        break
    else
        echo "Time required: $timeRequired and Time left: $timeLeft. Will conduct experiment."
    fi;

    ~/scripts/experiment.sh --experimentId $experimentId --dbMachine $dbMachine --dbPort $dbPort --dbUser $USER --serverMachine $serverMachine --clientMachine $clientMachine --waitInterval $waitInterval --pollInterval $pollInterval --poolSize $poolSize --messageSize $messageSize --noRandomClients $noRandomClients --noPairClients $noPairClients --noReqClients $noReqClients --noRepClients $noRepClients --remoteUserName $USER --clientRunTime $clientRunTime 
    echo "Experiment $experimentId completed"
done
