# Read the scenario description from the file scenario.txt

# Check the starting time (seconds since 1970 epoch)
startTime=`date +%s`
finishTime=`expr $startTime + 14280`
USER="user13"

sed 1d ~/scripts/scenario.txt | while read line
do
    echo $line
    # Columns are as follows
    # experimentId dbMachine dbPort server1 server2 waitInterval pollInterval poolSize messageSize noRandomClients clientRunTime
    experimentId=`echo $line | awk '{print $1}'`
    dbMachine=`echo $line | awk '{print $2}'`
    dbPort=`echo $line | awk '{print $3}'`
    server1=`echo $line | awk '{print $4}'` 
    server2=`echo $line | awk '{print $5}'` 
    waitInterval=`echo $line | awk '{print $6}'` 
    pollInterval=`echo $line | awk '{print $7}'` 
    poolSize=`echo $line | awk '{print $8}'`
    messageSize=`echo $line | awk '{print $9}'` 
    noRandomClients=`echo $line | awk '{print $10}'`  
    clientRunTime=`echo $line | awk '{print $11}'` 
    noPairClients=0
    noReqClients=0
    noRepClients=0
    #noPairClients=`echo $line | awk '{print $11}'` 
    #noReqClients=`echo $line | awk '{print $12}'`  
    #noRepClients=`echo $line | awk '{print $13}'` 
    
    # Start the new experiment only if sufficient time left
    currentTime=`date +%s`
    timeLeft=`expr $finishTime - $currentTime`
    timeRequired=`expr $clientRunTime + 30`
    if [ $timeLeft -lt $timeRequired ]
    then
        echo "Not enough time to run another experiment. Terminating..."
        break
    else
        echo "Time required: $timeRequired and Time left: $timeLeft. Will conduct experiment."
    fi;

    ~/scripts/multipleServerExperiment.sh --experimentId $experimentId --dbMachine $dbMachine --dbPort $dbPort --dbUser $USER --serverFirst $server1 --serverSecond $server2 --waitInterval $waitInterval --pollInterval $pollInterval --poolSize $poolSize --messageSize $messageSize --noRandomClients $noRandomClients --noPairClients $noPairClients --noReqClients $noReqClients --noRepClients $noRepClients --remoteUserName $USER --clientRunTime $clientRunTime 
    echo "Experiment $experimentId completed"
done
