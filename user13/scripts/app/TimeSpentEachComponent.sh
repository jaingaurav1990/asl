#!/bin/bash

expdirs=`find . ! -path . -maxdepth 1 -type d`
for dir in $expdirs
do
    cd $dir
    echo "Stepped into $dir"
    rm -f client.jar

    clientFiles=`ls client*`
    for file in $clientFiles
    do
        grep -h -i 'logging.RequestTracer$7' $file | awk -F: '{print $3}' > out.$file
    done

    formattedFiles=`ls out.client*`
    cat $formattedFiles > clientResponseTime.out

    rm out.client*
    # Collect time spent at client for sending message
    for file in $clientFiles
    do
        grep -h -i 'logging.RequestTracer$5' $file | awk -F: '{print $3}' > out.$file
    done
    formattedFiles=`ls out.client*`
    cat $formattedFiles > clientSendTime.out

    rm out.client*
    echo "Done collecting response time and send time at client..."

    serverFiles=`ls serverThread*`
    for file in $serverFiles
    do
        grep -h -i -w 'get' $file | awk -F: '{print $3}' > out.$file
    done

    formattedFiles=`ls out.serverThread*`
    cat $formattedFiles > serverResponseTime.out

    rm out.serverThread*
    for file in $serverFiles
    do  
       grep -h -i -w 'send' $file | awk -F: '{print $3}' > out.$file
    done
    formattedFiles=`ls out.serverThread*`
    cat $formattedFiles > serverSendTime.out
    rm out.serverThread*
    echo "Done collecting response time and send time component at Server..."

    dbFiles=`ls postgresql*`
    for file in $dbFiles
    do
        grep -h -i 'readOrGetMessage' $file | awk -F: '{print $6}' | sed 's/)$//' > out.$file
    done

    formattedFiles=`ls out.postgresql*`
    cat $formattedFiles > dbResponseTime.out
    rm out.postgresql*

    for file in $dbFiles
    do  
        grep -h -i 'insertMessageOne' $file | awk -F: '{print $6}' | sed 's/)$//' > out.$file
    done

    formattedFiles=`ls out.postgresql*`
    cat $formattedFiles > dbSendTime.out
    rm out.postgresql*

    echo "Done collecting data from DB logs"
    cd ..
    workingDir=`echo $dir | cut -b3-`
    id=`echo $workingDir | grep -o '[0-9]*'`

    # grep for the parameters for this particular experiment from the summary file
    parameters=`grep -w -A 1 "Experiment $id" SUMMARY | grep -v '\-\-' | tail -1 | grep -o '[0-9]*'`
    numClients=`echo $parameters | awk '{print $1}'`
    numConnections=`echo $parameters | awk '{print $2}'`
    msgSize=`echo $parameters | awk '{print $3}'`
    ./plotTimeSpentEachComponent.r $workingDir $id $numClients $numConnections $msgSize

done

echo "Done collecting data. Processing by R"

# Now there exists a short throughputSummary.txt file in each of the experiment
# directories. Merge all of them and put it in a format that can
# be read by R

summaryFiles=`find . -name sendTimeEachComponent.txt`
sort -n --merge $summaryFiles > sendTimeEachComponentOverallSummary.tmp
numLines=`cat sendTimeEachComponentOverallSummary.tmp | wc -l`
echo $numLines
numLines=`expr $numLines / 2 + 1`
cat sendTimeEachComponentOverallSummary.tmp | tail -$numLines > sendTimeEachComponentOverallSummary.txt
rm sendTimeEachComponentOverallSummary.tmp

summaryFiles=`find . -name responseTimeEachComponent.txt`
sort -n --merge $summaryFiles > responseTimeEachComponentOverallSummary.tmp
numLines=`cat responseTimeEachComponentOverallSummary.tmp | wc -l`
echo $numLines
numLines=`expr $numLines / 2 + 1`
cat responseTimeEachComponentOverallSummary.tmp | tail -$numLines > responseTimeEachComponentOverallSummary.txt
rm responseTimeEachComponentOverallSummary.tmp


#echo "Generating throughput summary..."
# ./summarizeThroughput.r


