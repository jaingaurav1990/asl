'''
Created on Dec 9, 2013

@author: gjain
'''
import optparse

def single_class_ms_mva(N, connections, msgSize, readServiceTime, writeServiceTime, waitTime, resultsFile):
    '''
    This function applies the MVA algorithm
    assuming a constant (load-independent) service 
    rate. 
    '''
    
    C = connections
    S = readServiceTime + writeServiceTime
    "Initializations before the iteration"
    X = (float(N)/(waitTime)) * 1000.0
    Q = N
    U = (float(X) * (S/1000.0))/float(C)
    " Correction Factor Initialization "
    raisedTo = 4.464 * (pow(C, 0.676) - 1)
    Y = 1
    
    R = 0
    while True:
        print "Correction Factor: " + str(Y)
        if N <= C:
            R = S
        else:
            R = S + (Y * S * (float(N - 1)/float(N)) * Q)

        Z = waitTime - R
        
        if Z >= 0:
            X = (float(N)/(Z + R)) * 1000
            print "N :" + str(N) + " R: " + str(R) + " Throughput: " + str(X) + " Service Time: " + str(S)
        else: 
            X = (float(N) * 1000)/R
            print "N :" + str(N) + " R: " + str(R) + " Throughput: " + str(X) + " Service Time: " + str(S)
        
        qPrev = Q
        uPrev = U
        Q = (X * R)/1000.0
        U = (X * S)/(1000.0 * C)
        if U > 1:
            U = 1
        print "Utilization: " + str(U)
        raisedTo = 4.464 * (pow(C, 0.676) - 1)
        yPrev = Y
        Y = pow(U, raisedTo)/C

        print "qPrev: " + str(qPrev) + " " + " Q:" + str(Q)
        diffQ = (abs(qPrev - Q))
        diffY = (abs(yPrev - Y))
        if U == 1 or (diffQ < 0.01 and diffY < 0.1):
            print "Solutions converged."
            break
        else:
            print "Solutions didn't converge. Iterating again..."

    f = open(resultsFile, 'a')
    f.write(str(N) + " " + str(connections) + " " + str(msgSize) + " " + str(X) + " " + str(R) + " " + str(Q) + "\n") 
    f.close()
        
def main():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--infile', help = 'File containing raw data', dest = 'inFile', \
                      type = 'string', action = 'store')
    parser.add_option('-o', '--outfile', help = 'File to save modelling results', dest = 'outFile', \
                      type = 'string', action = 'store')
    
    (opts, args) = parser.parse_args()
    
    if opts.outFile is not None:
        resultsFile = opts.outFile
    else:
        print "-o option must be specified. Exiting ..."
        exit(1)
    
    if opts.inFile is not None:
        inFile = opts.inFile
    else:
        print "-i option must be specified. Exiting ..."
        exit(1)
    
    # Print header in resultsFile
    f = open(resultsFile, 'w')
    f.write("Clients Connections MsgSize Pthroughput   Presponse    Pq\n")
    f.close()

    # Read parameters from inFile and call mva
    f = open(inFile, 'r')
    for line in f:
        print line
    f.close()
    counter = 0
    
    with open(inFile, 'r') as fp:
        for line in fp:
            if counter <= 1: # First two lines are headers
                counter += 1
                continue
            line = line.strip()
            params = line.split(None, 5)
            print params
            clients = int(params[0])
            connections = int(params[1])
            msgSize = params[2]
            sendServiceTime = float(params[3])
            readServiceTime = float(params[4])
            
            single_class_ms_mva(clients, connections, msgSize, readServiceTime, sendServiceTime, 100, resultsFile)
        
if __name__ == '__main__':
    main()