'''
Created on Dec 9, 2013

@author: gjain
'''
import optparse

def single_class_mva_q_approx(N, connections, msgSize, readServiceTime, writeServiceTime, DCsend, DCread, waitTime, resultsFile):
    '''
    This function applies the MVA algorithm
    assuming a constant (load-independent) service 
    rate. 
    '''
    DCService = DCsend + DCread
    fcfsReadS = (readServiceTime/connections);
    fcfsWriteS = (writeServiceTime/connections);
    fcfsService = fcfsReadS + fcfsWriteS
    
    ASreadS = (readServiceTime * (connections - 1))/connections
    ASwriteS = (writeServiceTime * (connections - 1))/connections
    ASService = ASreadS + ASwriteS
    
    Q = N
       
    R = {}
    R['fcfs'] = {}
    R['AS'] = {}
    
    X = 0
    while True:
        R['fcfs'] = fcfsService * (1.0 + (float(N - 1)/float(N)) * Q)
        R['AS'] = ASService
        TotalR = R['fcfs'] + R['AS'] + DCService
    
        Z = waitTime - TotalR
        
        if Z >= 0:
            X = (float(N)/(Z + TotalR)) * 1000
            print "N :" + str(N) + " TotalR: " + str(TotalR) + " Throughput: " + str(X)
        else: 
            X = (float(N) * 1000)/TotalR
            print "N :" + str(N) + " TotalR: " + str(TotalR) + " Throughput: " + str(X)
        
        qPrev = Q
        Q = (X * R['fcfs'])/1000.0
        diff = (abs(qPrev - Q))
        if diff < 0.001:
            print "Solutions converged."
            break
        else:
            print "Solutions didn't converge. Iterating again..."

    f = open(resultsFile, 'a')
    f.write(str(N) + " " + str(connections) + " " + str(msgSize) + " " + str(X) + " " + str(TotalR) + " " + str(Q) + "\n") 
    f.close()
        
def main():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--infile', help = 'File containing raw data', dest = 'inFile', \
                      type = 'string', action = 'store')
    parser.add_option('-o', '--outfile', help = 'File to save modelling results', dest = 'outFile', \
                      type = 'string', action = 'store')
    
    (opts, args) = parser.parse_args()
    
    if opts.outFile is not None:
        resultsFile = opts.outFile
    else:
        print "-o option must be specified. Exiting ..."
        exit(1)
    
    if opts.inFile is not None:
        inFile = opts.inFile
    else:
        print "-i option must be specified. Exiting ..."
        exit(1)
    
    # Print header in resultsFile
    f = open(resultsFile, 'w')
    f.write("Clients Connections MsgSize Pthroughput   Presponse    Pq\n")
    f.close()

    # Read parameters from inFile and call mva
    f = open(inFile, 'r')
    for line in f:
        print line
    f.close()
    counter = 0
    
    with open(inFile, 'r') as fp:
        for line in fp:
            if counter <= 1: # First two lines are headers
                counter += 1
                continue
            line = line.strip()
            params = line.split(None, 7)
            print params
            clients = int(params[0])
            connections = int(params[1])
            msgSize = params[2]
            sendServiceTime = float(params[3])
            readServiceTime = float(params[4])
            DCsend = float(params[5])
            DCread = float(params[6])
            
            single_class_mva_q_approx(clients, connections, msgSize, readServiceTime, sendServiceTime, DCsend, DCread, 100, resultsFile)
        
if __name__ == '__main__':
    main()