'''
Created on Dec 9, 2013

@author: gjain
'''
import optparse

def mva(N, connections, msgSize, readServiceTime, writeServiceTime, DCSend, DCRead, waitTime, resultsFile):
    '''
    This function applies the MVA algorithm
    assuming a constant (load-independent) service 
    rate. 
    '''
    
    fcfsReadS = (readServiceTime/connections);
    fcfsWriteS = (writeServiceTime/connections);
    
    ASreadS = (readServiceTime * (connections - 1))/connections
    ASwriteS = (writeServiceTime * (connections - 1))/connections
    
    Q = {}
       
    Q['read'] = N
    Q['write'] = N
    
    A = {}
    R = {}
    R['fcfs'] = {}
    R['AS'] = {}
    
    X = 0
    while True:
        A['read'] = ((N - 1)/float(N)) * Q['read'] + Q['write']
        A['write'] = ((N - 1)/float(N)) * Q['write'] + Q['read']
        
        R['fcfs']['read'] = fcfsReadS * (1 + (float(N - 1)/float(N))*Q['read'])  + fcfsWriteS * (Q['write'])
        R['AS']['read'] = ASreadS
        R['fcfs']['write'] = fcfsWriteS * (1 + (float(N - 1)/float(N))*Q['write'])  + fcfsReadS * (Q['read'])
        R['AS']['write'] = ASwriteS
        
        readR = R['fcfs']['read'] + R['AS']['read'] + DCRead
        writeR = R['fcfs']['write'] + R['AS']['write'] + DCSend
        totalR = (readR + writeR) 
        Z = waitTime - totalR
        
        if Z >= 0:
            X = (float(N)/(Z + totalR)) * 1000
            print "N :" + str(N) + " readR:" + str(readR) + " writeR: " + str(writeR) + " totalR: " + str(totalR) + " Throughput: " + str(X)
        else: 
            X = (float(N) * 1000)/totalR
            print "N :" + str(N) + " readR:" + str(readR) + " writeR: " + str(writeR) + " totalR: " + str(totalR) + " Throughput: " + str(X)
        
        qPrevRead = Q['read']
        qPrevWrite = Q['write']
        
        Q['read'] = (X * (readR))/1000.0
        Q['write'] = (X * (writeR))/1000.0
        
        print "readQ: " + str(Q['read']) + " writeQ: " + str(Q['write'])
        diff1 = (abs(Q['read'] - qPrevRead))
        diff2 = (abs(Q['write'] - qPrevWrite))
        
        if diff1 <= 0.001 and diff2 <= 0.001:
            print "Solutions converged. Breaking from loop..."
            break
        else:
            print "Solution didn't converge. Looping..."
    
    readResponse = R['fcfs']['read'] + R['AS']['read'] + DCRead
    writeResponse = R['fcfs']['write'] + R['AS']['write'] + DCSend
    
    f = open(resultsFile, 'a')
    f.write(str(N) + " " + str(connections) + " " + str(msgSize) + " " + str(X) + " " + str(writeResponse) + " " + 
            str(readResponse) + " " + str(Q['write']) + " " + str(Q['read']) + "\n")
    f.close()
            
def main():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--infile', help = 'File containing raw data', dest = 'inFile', \
                      type = 'string', action = 'store')
    parser.add_option('-o', '--outfile', help = 'File to save modelling results', dest = 'outFile', \
                      type = 'string', action = 'store')
    
    (opts, args) = parser.parse_args()
    
    if opts.outFile is not None:
        resultsFile = opts.outFile
    else:
        print "-o option must be specified. Exiting ..."
        exit(1)
    
    if opts.inFile is not None:
        inFile = opts.inFile
    else:
        print "-i option must be specified. Exiting ..."
        exit(1)
    
    # Print header in resultsFile
    f = open(resultsFile, 'w')
    f.write("Clients Connections MsgSize PThroughput Psend Pread readQ writeQ\n")
    f.close()

    # Read parameters from inFile and call mva
    f = open(inFile, 'r')
    for line in f:
        print line
    f.close()
    counter = 0
    
    with open(inFile, 'r') as fp:
        for line in fp:
            if counter <= 1: # First two lines are headers
                counter += 1
                continue
            line = line.strip()
            params = line.split(None, 7)
            print params
            clients = int(params[0])
            connections = int(params[1])
            msgSize = params[2]
            sendServiceTime = float(params[3])
            readServiceTime = float(params[4])
            DCsend = float(params[5])
            DCread = float(params[6])
            
            mva(clients, connections, msgSize, readServiceTime, sendServiceTime, DCsend, DCread, 100, resultsFile)
        
if __name__ == '__main__':
    main()