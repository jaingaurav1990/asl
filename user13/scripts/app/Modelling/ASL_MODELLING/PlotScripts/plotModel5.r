#!/usr/bin/Rscript
library(ggplot2)
library(reshape)
library(gridExtra)

args <- commandArgs(TRUE);
# Very first argument is the directory containing modelling results
setwd(args[1])

df = read.table('model5_results.txt', header = TRUE)
df_actual = read.table('measured_results_model1.txt', header = TRUE)
dfMerge = merge(df, df_actual, by = c("Clients", "MsgSize", "Connections"))

# All graphs should be stored in the directory pointed by second argument
setwd(args[2])

# Plot the results obtained from model for msgSize = 1, 1999
# and connections = 10, 15, 20, 25, 30, 50

msgSizes = c(1999)
connVals = c(1, 2, 4, 5, 8, 10, 15, 20, 30, 50)

cmp_df = data.frame(dfMerge$Clients, dfMerge$MsgSize, dfMerge$Connections, dfMerge$PThroughput, dfMerge$MThroughput)
colnames(cmp_df) = c("Clients", "MsgSize", "Connections", "ModelThroughput", "MeasuredThroughput")
cmp_df_melt = melt(cmp_df, id = c("Clients", "MsgSize", "Connections"))
colnames(cmp_df_melt) = c("Clients", "MsgSize", "Connections", "Variable", "Value")

cmp_df_send = data.frame(dfMerge$Clients, dfMerge$MsgSize, dfMerge$Connections, dfMerge$Psend, dfMerge$Msend)
colnames(cmp_df_send) = c("Clients", "MsgSize", "Connections", "Model", "Measured")
send_melt = melt(cmp_df_send, id = c("Clients", "MsgSize", "Connections"))
colnames(send_melt) = c("Clients", "MsgSize", "Connections", "Variable", "Value")

cmp_df_read = data.frame(dfMerge$Clients, dfMerge$MsgSize, dfMerge$Connections, dfMerge$Pread, dfMerge$Mread)
colnames(cmp_df_read) = c("Clients", "MsgSize", "Connections", "Model", "Measured")
read_melt = melt(cmp_df_read, id = c("Clients", "MsgSize", "Connections"))
colnames(read_melt) = c("Clients", "MsgSize", "Connections", "Variable", "Value")


dflist = list(cmp_df_melt, send_melt, read_melt)
plotList = list()
sendList = list()
readList = list()
#print (length(dflist))

for (k in 1:length(dflist)) { 
    curr_df = dflist[[k]]
    for (i in msgSizes) {
	msg_Subset = subset(curr_df, MsgSize == i)
	
	for (j in connVals) {
		plotData = subset(msg_Subset, Connections == j)
		
        if (k == 1) {
            filename = paste("TPModelling.",i, ".", j, ".png", sep = '') 
            list_item = ggplot(data = plotData, aes(Clients, y = Value, color = Variable)) +
                geom_point() +
                geom_line() + 
                xlab("Number of Clients") +
                ylab("Throughput (in jobs/second)") +
                labs(title = paste("Message Size = ", i, ", Connections = ", j), color = "Legend")
            
            plotList = c(plotList, list(list_item))
        }
        else if (k == 2) {
            filename = paste("SendModelling.",i, ".", j, ".png", sep = '') 
            list_item = ggplot(data = plotData, aes(Clients, y = Value, color = Variable)) +
                geom_point() +
                geom_line() + 
                xlab("Number of Clients") +
                ylab("Time to send a message (in ms)") +
                labs(title = paste("Message Size = ", i, ", Connections = ", j), color = "Legend")
        
            sendList = c(sendList, list(list_item))
        }
        else {
            filename = paste("ReadModelling.",i, ".", j, ".png", sep = '') 
            list_item = ggplot(data = plotData, aes(Clients, y = Value, color = Variable)) +
                geom_point() +
                geom_line() + 
                xlab("Number of Clients") +
                ylab("Time to read a message (in ms)") +
                labs(title = paste("Message Size = ", i, ", Connections = ", j), color = "Legend")
        
            readList = c(readList, list(list_item))

        }
			
	    }
    }
}

# Plot all graphs in a single grid
#pdf("Summary.pdf")
listPlotList = list(plotList, sendList, readList)
for (k in 1:length(listPlotList)) {
    currList = listPlotList[[k]]
    filename = ""
    if (k == 1) {
        filename = "ThroughputModelling.pdf"
    }
    else if (k == 2) {
        filename = "SendModelling.pdf"
    }
    else {
        filename = "ReadModelling.pdf"
    }

    plot = list()
    counter = 1

    pdf(filename, width = 14)
    for (i in 1:length(currList)) {
        plot[[counter]] = currList[[i]]
        if (counter %% 4 == 0) {
            print (do.call(grid.arrange, c(plot, ncol = 2, nrow = 2)))
            plot = list()
            counter = 0
        }
        counter = counter + 1
    }
    if (length(plot) != 0) {
        print(do.call(grid.arrange, c(plot, ncol = 2, nrow = 2)))
    }
    dev.off()
}



