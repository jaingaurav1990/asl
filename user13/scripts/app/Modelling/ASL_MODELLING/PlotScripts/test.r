#!/usr/bin/Rscript
library(ggplot2)
library(reshape)
library(gridExtra)

args <- commandArgs(TRUE);
# Very first argument is the directory containing modelling results
setwd(args[1])

df = read.table('model_results.txt', header = TRUE)
df_actual = read.table('measured_results.txt', header = TRUE)
dfMerge = merge(df, df_actual, by = c("Clients", "MsgSize", "Connections"))

# All graphs should be stored in the directory pointed by second argument
setwd(args[2])

# Plot the results obtained from model for msgSize = 1, 1999
# and connections = 10, 15, 20, 25, 30, 50

msgSizes = c(1, 1999)
connVals = c(5, 10, 15, 20, 25, 30, 50)

cmp_df = data.frame(dfMerge$Clients, dfMerge$MsgSize, dfMerge$Connections, dfMerge$PThroughput, dfMerge$MThroughput)
colnames(cmp_df) = c("Clients", "MsgSize", "Connections", "ModelThroughput", "MeasuredThroughput")
cmp_df_melt = melt(cmp_df, id = c("Clients", "MsgSize", "Connections"))
colnames(cmp_df_melt) = c("Clients", "MsgSize", "Connections", "Variable", "Value")

plotList = list()
for (i in msgSizes) {
	msg_Subset = subset(cmp_df_melt, MsgSize == i)
	
	for (j in connVals) {
		plotData = subset(msg_Subset, Connections == j)
		
		filename = paste("Modelling.",i, ".", j, ".png", sep = '') 
		list_item = ggplot(data = plotData, aes(Clients, y = Value, color = Variable)) +
			geom_point() +
			geom_line() + 
			xlab("Number of Clients") +
			ylab("Throughput (in ops/second)") +
			labs(title = paste("Message Size = ", i, ", Connections = ", j), color = "Legend")
		
		plotList = c(plotList, list_item)
		ggsave(file = filename)
			
	}
}

x <- qplot(mpg, disp, data = mtcars)
y <- qplot(hp, wt, data = mtcars)
#grid.arrange(x, y)
lapply(plotList, class)
grid.arrange(plotList[1], plotList[2])

#mylist = list(plotList[1], plotList[2])
#do.call(grid.arrange, mylist)


