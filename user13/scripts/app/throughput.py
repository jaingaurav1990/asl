#!/usr/bin/python

import sys, getopt

def processFile(outfile, infile, interval):
    print "Processing " + infile
    fileEnd = False
    throughputFile = open(outfile, 'w')
    f = open(infile)
    while fileEnd == False:
        line = f.readline()
        if line == '':
            break
        fields = line.split(None, 3)
        beginTS = fields[0]
        for x in range(0, interval-1):
            line = f.readline()
            if line == '':
                fileEnd = True
                break

        if line != '':
            fields = line.split(None, 3)
            endTS = fields[0]
            throughput = (int(interval)*1000)/(int(endTS) - int(beginTS))
            throughputFile.write(str(throughput) + "\n")

    f.close()
    throughputFile.close()

def main(argv):
    try: 
        opts, args = getopt.getopt(argv, "p:f:")
    except getopt.GetoptError:
        print 'Usage: throughput.py -p <interval> -f <tempThroughputFile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-f':
            infile = arg
        if opt == '-p':
            interval = arg

    processFile("Throughput.out", infile, int(interval))

if __name__ == "__main__":
    main(sys.argv[1:])





    

