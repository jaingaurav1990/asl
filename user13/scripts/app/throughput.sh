#!/bin/bash

serverLogs=`ls serverThread*`

for file in $serverLogs
do
    echo "Formatting file " $file
    grep -h 'send' $file | awk -F: '{print $1 " " $2 " " $3}' > out.$file
done

formattedFiles=`ls out.serverThread*`
sort --merge $formattedFiles > Throughput.temp

python throughput.py -p 100 -f Throughput.temp
