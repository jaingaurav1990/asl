#!/bin/bash

# This script reads through the SUMMARY file and prepares a nice
# summary of the experiments conducted

id=24
grep -A 1 "Experiment $id" SUMMARY | grep -v '\-\-' | tail -1 | grep -o '[0-9]*' | head -3 | tail -1
