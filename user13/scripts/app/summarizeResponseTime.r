#!/usr/bin/Rscript
library(ggplot2)
df <- read.table("respOverallSummary.txt", header = TRUE)

msg_10 = subset(df, df$msgSize == 10) 
if (nrow(msg_10) > 0) {
    limits = aes(ymax = msg_10$Mean + msg_10$Deviation, ymin = msg_10$Mean - msg_10$Deviation)
    ggplot(data = msg_10, aes(clients, Mean, color = as.factor(connections), group = as.factor(connections))) +
    geom_point() + geom_line() +
    geom_errorbar(limits) +
    xlab("Number of Clients") +
    ylab("Response Time (in ms)") + 
    labs(title = "Message size 10 bytes", color = "Connections") 
    ggsave(file = "responseTimeMessageSize10.png")
}

msg_1999 = subset(df, df$msgSize == 1999)
if (nrow(msg_1999) > 0) {
    limits = aes(ymax = msg_1999$Mean + msg_1999$Deviation, ymin = msg_1999$Mean - msg_1999$Deviation)
    ggplot(data = msg_1999, aes(clients, Mean, color = as.factor(connections), group = as.factor(connections))) +
    geom_point() + geom_line() +
    geom_errorbar(limits) +
    xlab("Number of Clients") +
    ylab("Resposne Time (in ms)") +
    coord_cartesian(xlim = c(0, 700)) +
    scale_x_continuous(breaks = c(10, 50, 100, 200, 400, 600)) +
    labs(title = "Message size 1999 bytes", color = "Connections") 
    ggsave(file = "responseTimeMessageSize1999.png")
}

msg_1000000 = subset(df, df$msgSize == 1000000)
if (nrow(msg_1000000) > 0) {
    limits = aes(ymax = msg_1000000$Mean + msg_1000000$Deviation, ymin = msg_1000000$Mean - msg_1000000$Deviation)
    ggplot(data = msg_1000000, aes(clients, Mean, color = as.factor(connections), group = as.factor(connections))) +
    geom_point() + geom_line() +
    geom_errorbar(limits) +
    xlab("Number of Clients") +
    ylab("Response Time (in ms)") +
    coord_cartesian(xlim = c(0, 700)) +
    scale_x_continuous(breaks = c(10, 50, 100, 200, 400, 600)) +
    labs(title = "Message size 1000000 bytes", color = "Connections") +
    ggsave(file = "responseTimeMessageSize1000000.png")
}

