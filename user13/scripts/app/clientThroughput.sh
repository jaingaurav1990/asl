#!/bin/bash

# Calculate throughput as seen by the clients

expdirs=`find . ! -path . -maxdepth 1 -type d`
for dir in $expdirs
do
    cd $dir
    echo "Stepped into $dir"

    rm -f client.jar

    clientFiles=`ls client*`
    for file in $clientFiles
    do
        grep -h -i "\$5" $file | awk -F: '{print $1""$3}' > out.$file
    done

    formattedFiles=`ls out.*`
    sort -n -t " " -k1,1 $formattedFiles > clientThroughput.out
    sort -n -c clientThroughput.out

    rm out.*

    # Subtract the value in very first row of very first column  
    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' clientThroughput.out > absTimeClientThroughput.out

    python ../clientThroughput.py -i 1000 -f absTimeClientThroughput.out
    workingDir=`echo $dir | cut -b3-`
    id=`echo $workingDir | grep -o '[0-9]*'`

    cd ..
    parameters=`grep -w -A 1 "Experiment $id" SUMMARY | grep -v '\-\-' | tail -1 | grep -o '[0-9]*'`
    numClients=`echo $parameters | awk '{print $1}'`
    numConnections=`echo $parameters | awk '{print $2}'`
    msgSize=`echo $parameters | awk '{print $3}'`

    ./plotThroughput.r $workingDir $id $numClients $numConnections $msgSize
done

# Now there exists a short throughputSummary.txt file in each of the experiment
# directories. Merge all of them and put it in a format that can
# be read by R

summaryFiles=`find . -name throughputSummary.txt`
sort -n --merge $summaryFiles > throughputOverallSummary.tmp
numLines=`cat throughputOverallSummary.tmp | wc -l`
echo $numLines
numLines=`expr $numLines / 2 + 1`
cat throughputOverallSummary.tmp | tail -$numLines > throughputOverallSummary.txt
rm throughputOverallSummary.tmp

echo "Generating throughput summary..."
./summarizeThroughput.r

