#!/bin/bash

# The -d option specifies the directory into which
# the logs should be downloaded. 
logDir=""
startId=""
lastId=""
userName=""
remoteHost=""
logDirSpecified=false
startIdSpecified=false
lastIdSpecified=false
userNameSpecified=false
remoteHostSpecified=false

while getopts ":d:f:l:u:h:" opt; do
    case $opt in
        d)
            logDir="$OPTARG"
            logDirSpecified=true
            ;;
	f)
            startId=$OPTARG
            startIdSpecified=true
            ;;
	l)
            lastId=$OPTARG
            lastIdSpecified=true
            ;;
	u)
            userName=$OPTARG
            userNameSpecified=true
            ;;
	h)
            remoteHost=$OPTARG
            remoteHostSpecified=true
            ;;
        /?)
            echo "Invalid option"
            exit 1
            ;;
        :)
            echo "Option $opt requires argument"
            exit 1
            ;;
    esac
done

if ! $logDirSpecified || ! $startIdSpecified || ! $lastIdSpecified || ! $userNameSpecified || ! $remoteHostSpecified
then
    echo "Usage: `basename $0` -d <Directory where to download logs> -f <Experiment Id of the first experiment>  \
		-l <Experiment-id of the last experiment> -u <user name on remote machine> -h <remote-host>"
    exit 1
fi;

remotePathPrefix="$userName@$remoteHost:/mnt/asl/$userName/"
id=$startId
while [ $id -le $lastId ];
do
    remotePath="$remotePathPrefix"
    remotePath+="exp$id"
    echo "Downloading logs from $remotePath"
    scp -r $remotePath $logDir
    id=`expr $id + 1`
done
