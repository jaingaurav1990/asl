#!/bin/bash

###############################
#
# Read command line arguments
#
###############################

set -x
function usage() {
    local programName=$1
        echo "Usage: $programName --serverMachine=<address> --clientMachine=<address> --waitInterval=<int> --pollInterval=<int> --messageSize=<int>--noRandomClients=<int> --noPairClients=<int> --noReqClients=<int> --noRepClients=<int> --remoteUserName=<username> --experimentId=<id> --clientRunTime=<seconds> --dbMachine=<address> --dbPort=<int>"
            exit -1
        }

serverMachine=""
poolSize=""
clientMachine=""
waitInterval=""
pollInterval=""
messageSize=""
noRandomClients=""
noPairClients=""
noOfClients=""
remoteUserName=""
experimentId=""
dbMachine=""
dbPort=""
dbUser=""
clientRunTime=5

# Extract command line arguments
TEMP=`getopt -o b: --long serverMachine:,poolSize:,clientMachine:,waitInterval:,pollInterval:,messageSize:,noRandomClients:,noPairClients:,noReqClients:,noRepClients:,remoteUserName:,experimentId:,clientRunTime:,dbMachine:,dbPort:,dbUser: \
         -n 'example.bash' -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi
eval set -- "$TEMP"

while true ; do
        case "$1" in
                --serverMachine) serverMachine="$2" ; shift 2 ;;
                --poolSize) poolSize="$2" ; shift 2 ;;
                --clientMachine) clientMachine="$2" ; shift 2 ;;
                --waitInterval) waitInterval="$2" ; shift 2 ;;
                --pollInterval) pollInterval="$2" ; shift 2 ;;
                --messageSize) messageSize="$2" ; shift 2 ;;
                --noRandomClients) noRandomClients="$2" ; shift 2 ;; 
                --noPairClients) noPairClients="$2" ; shift 2 ;;
                --noReqClients) noReqClients="$2" ; shift 2 ;;
                --noRepClients) noRepClients="$2" ; shift 2 ;;
                --remoteUserName) remoteUserName="$2" ; shift 2 ;;
                --experimentId) experimentId="$2" ; shift 2 ;;
                --clientRunTime) clientRunTime="$2" ; shift 2 ;;
                --dbMachine) dbMachine="$2"; shift 2 ;;
                --dbPort) dbPort="$2"; shift 2 ;;
                --dbUser) dbUser="$2"; shift 2;;
                --) shift ; break ;;
                *) echo "Internal error!" ; exit 1 ;;
        esac
done

# Check for correctness of the commandline arguments
if [[ $serverMachine == "" || $poolSize == "" || $clientMachine == "" || $waitInterval == "" || $pollInterval == "" || $messageSize == "" || $noRandomClients == "" || $noPairClients == "" ||  $noReqClients == "" || $noRepClients == "" || $remoteUserName == "" || $experimentId == "" || $dbMachine == "" || $dbPort == ""  || dbUser == "" ]]
then
	usage $1
fi

#####################################
#
# Copy server and clients to machines
#
#####################################

echo $serverMachine
echo $clientMachine
remoteHome='/home/'
remoteHome+=$remoteUserName
                                                                                  
echo $remoteHome
echo "  Copying server.jar to server machine: $serverMachine ... "
# Copy jar to server machine
scp CodeJar/AslTest.jar $remoteUserName@$serverMachine:$remoteHome
echo "  Copying client.jar to client machine: $clientMachine ... "
# Copy jar to client machine
scp CodeJar/AslTest.jar $remoteUserName@$clientMachine:$remoteHome

######################################
#
# Set up the database machine
#
######################################

ssh $remoteUserName@$dbMachine "cd ~/scripts/db/; ./postgresql start;"
sleep 5
ssh $remoteUserName@$dbMachine "cd ~/scripts/db/; ~/db/pgsql/bin/psql -p $dbPort -d asl_hs13_g13 -h localhost -f DBscheme.sql"

######################################
#
# Run server and clients
#
######################################

expectedConnections=`expr $noRandomClients + $noPairClients + $noPairClients + $noReqClients + $noRepClients + 1`
pids=""
# Run server
echo "  Starting the server"
ssh $remoteUserName@$serverMachine "java -jar $remoteHome/AslTest.jar $dbMachine $dbPort $dbUser $poolSize $expectedConnections 2>&1 > /tmp/$remoteUserName.server.out " &
pids="$pids $!"
# Wait for the server to start up
echo -ne "  Waiting for the server to start up..."
sleep 1
while [ `ssh $remoteUserName@$serverMachine "cat /tmp/$remoteUserName.server.out | grep 'Server listening' | wc -l"` != 1 ]
do
        sleep 1
done
echo "OK"

echo "  Start the clients on the client machine: $clientMachine"
# Run RandomClients
if [ $noRandomClients -ne 0 ];
then
    ssh $remoteUserName@$clientMachine "cd $remoteHome; java -cp AslTest.jar client.RandomClientLaunch $serverMachine 3456 $noRandomClients $clientRunTime true $waitInterval $pollInterval 1 $messageSize 2>&1 > /tmp/out.$remoteUserName.RandomClientLauncher" &
    pids="$pids $!"

    # Check that all random clients have been launched
    echo -ne " Waiting for all random clients to be launched..."
    sleep 1
    while [ `ssh $remoteUserName@$clientMachine "cat /tmp/out.$remoteUserName.RandomClientLauncher | grep 'RandomClientLauncher finished launching ' | wc -l"` != 1 ]
    do
        sleep 1
    done
    echo "OK"
fi;

if [ $noPairClients -ne 0 ]
then
pairClientStart=`expr $noRandomClients + 1`
# Run PairClients
ssh $remoteUserName@$clientMachine "cd $remoteHome; java -cp AslTest.jar client.PairClientLaunch $serverMachine 3456 $noPairClients $clientRunTime $waitInterval $pollInterval true $pairClientStart > /tmp/out.$remoteUserName.PairClientLauncher" &
pids="$pids $!"

# Check that all pair clients have been launched
echo -ne " Waiting for all pair clients to be launched..."
sleep 1
while [ `ssh $remoteUserName@$clientMachine "cat /tmp/out.$remoteUserName.PairClientLauncher | grep 'PairClientLauncher finished launching ' | wc -l"` != 1 ]
do
    sleep 1
done
echo "OK"
fi;

if [ $noReqClients -ne 0 ]
then
# Run ReqResp Clients
ssh $remoteUserName@$clientMachine "cd $remoteHome; java -cp AslTest.jar client.ReqRespClientLaunch $serverMachine 3456 $noReqClients $noRepClients $clientRunTime $waitInterval $pollInterval true > /tmp/out.$remoteUserName.ReqRespClientLauncher" &
pids="$pids $!"

# Check that all ReqResp clients have been launched
echo -ne " Waiting for all ReqResp clients to be launched..."
sleep 1
while [ `ssh $remoteUserName@$clientMachine "cat /tmp/out.$remoteUserName.ReqRespClientLauncher | grep 'ReqRespClientLauncher finished launching ' | wc -l"` != 1 ]
do
        sleep 1
done
echo "OK"
fi;

# Wait for the clients and server to finish
echo -ne "  Waiting for the clients and server to finish ... "
for f in $pids
do
        wait $f
done
echo "OK"

# Make a directory on /mnt/asl for logs
echo "Creating log directory for experiment $experimentId"
logdir="exp"
logdir+=$experimentId
ssh $remoteUserName@$clientMachine "mkdir -p /mnt/asl/$remoteUserName/$logdir"
echo "Copying logs on client..."
ssh $remoteUserName@$clientMachine "mv /tmp/client* /mnt/asl/$remoteUserName/$logdir"
echo "Copying logs on server..."
ssh $remoteUserName@$serverMachine "mv /tmp/serverThread* /mnt/asl/$remoteUserName/$logdir"

echo "Stopping database server on DB machine"
ssh $remoteUserName@$dbMachine "cd ~/scripts/db/; ./postgresql stop;"
echo "Copying logs on DB machine..."
ssh $remoteUserName@$dbMachine "mv ~/db/pgsql/data/pg_log/postgresql* /mnt/asl/$remoteUserName/$logdir"



echo "Done"
