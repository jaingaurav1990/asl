#!/bin/bash

clientFiles=`ls client*`
echo $clientFiles

for file in $clientFiles
do
    echo "Formatting file " $file
    grep -h -i 'thinktime\|$5' $file | awk -F: '{print $1 " " $2 " " $3}' > out.$file
done

# Compute response time for each individual client using python script
formattedFiles=`ls out.*`

for file in $formattedFiles
do
    python responseTime.py -f $file
done

# Merge sorted file in one big file
sortedFiles=`ls resp.out.*`
sort --merge $sortedFiles > ResponseTime.out

# Generate a plot
gnuplot << EOF
set terminal jpeg
set output 'ResponseTime.jpg'
set xlabel 'Message Number (s)'
set ylabel 'Response Time (ms)'
set title 'Trace log'
set xrange[0:]
plot 'ResponseTime.out' using 2 with lp title "Experiment"
EOF

