#!/bin/bash

###############################
#
# Read command line arguments
#
###############################

set -x
function usage() {
    local programName=$1
        echo "Usage: $programName --serverFirst=<address> --serverSecond=<address> --clientMachine=<address> --waitInterval=<int> --pollInterval=<int> --messageSize=<int>--noRandomClients=<int> --noPairClients=<int> --noReqClients=<int> --noRepClients=<int> --remoteUserName=<username> --experimentId=<id> --clientRunTime=<seconds> --dbMachine=<address> --dbPort=<int>"
            exit -1
        }

serverFirst=""
serverSecond=""
poolSize=""
clientMachine=""
waitInterval=""
pollInterval=""
messageSize=""
noRandomClients=""
noPairClients=""
noOfClients=""
remoteUserName=""
experimentId=""
dbMachine=""
dbPort=""
dbUser=""
clientRunTime=5

# Extract command line arguments
TEMP=`getopt -o b: --long serverFirst:,serverSecond:,poolSize:,clientMachine:,waitInterval:,pollInterval:,messageSize:,noRandomClients:,noPairClients:,noReqClients:,noRepClients:,remoteUserName:,experimentId:,clientRunTime:,dbMachine:,dbPort:,dbUser: \
         -n 'example.bash' -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi
eval set -- "$TEMP"

while true ; do
        case "$1" in
                --serverFirst) serverFirst="$2" ; shift 2 ;;
                --serverSecond) serverSecond="$2" ; shift 2 ;;
                --poolSize) poolSize="$2" ; shift 2 ;;
                --clientMachine) clientMachine="$2" ; shift 2 ;;
                --waitInterval) waitInterval="$2" ; shift 2 ;;
                --pollInterval) pollInterval="$2" ; shift 2 ;;
                --messageSize) messageSize="$2" ; shift 2 ;;
                --noRandomClients) noRandomClients="$2" ; shift 2 ;; 
                --noPairClients) noPairClients="$2" ; shift 2 ;;
                --noReqClients) noReqClients="$2" ; shift 2 ;;
                --noRepClients) noRepClients="$2" ; shift 2 ;;
                --remoteUserName) remoteUserName="$2" ; shift 2 ;;
                --experimentId) experimentId="$2" ; shift 2 ;;
                --clientRunTime) clientRunTime="$2" ; shift 2 ;;
                --dbMachine) dbMachine="$2"; shift 2 ;;
                --dbPort) dbPort="$2"; shift 2 ;;
                --dbUser) dbUser="$2"; shift 2;;
                --) shift ; break ;;
                *) echo "Internal error!" ; exit 1 ;;
        esac
done

# Check for correctness of the commandline arguments
if [[ $serverFirst == "" || $serverSecond == "" || $poolSize == "" || $clientMachine == "" || $waitInterval == "" || $pollInterval == "" || $messageSize == "" || $noRandomClients == "" || $noPairClients == "" ||  $noReqClients == "" || $noRepClients == "" || $remoteUserName == "" || $experimentId == "" || $dbMachine == "" || $dbPort == ""  || dbUser == "" ]]
then
	usage $1
fi

#####################################
#
# Copy server and clients to machines
#
#####################################

echo $serverMachine
echo $clientMachine
remoteHome='/home/'
remoteHome+=$remoteUserName
                                                                                  
echo $remoteHome
echo "  Copying server.jar to server machine: $serverMachine ... "
# Copy jar to server machine
#scp CodeJar/AslTest.jar $remoteUserName@$serverFirst:$remoteHome
#scp CodeJar/AslTest.jar $remoteUserName@$serverSecond:$remoteHome
echo "  Copying client.jar to client machine: $clientMachine ... "
# Copy jar to client machine
#scp CodeJar/AslTest.jar $remoteUserName@$clientMachine:$remoteHome

######################################
#
# Set up the database machine
#
######################################

ssh $remoteUserName@$dbMachine "cd ~/scripts/db/; ./postgresql start;"
sleep 5
ssh $remoteUserName@$dbMachine "cd ~/scripts/db/; ~/db/pgsql/bin/psql -p $dbPort -d asl_hs13_g13 -h localhost -f DBscheme.sql"

######################################
#
# Run server and clients
#
######################################

halfClients=`expr $noRandomClients / 2`
expectedConnectionsFirst=`expr $halfClients + 1`
expectedConnectionsSecond=$halfClients
pids=""
# Run first server
echo "  Starting the first server"
ssh $remoteUserName@$serverFirst "java -jar $remoteHome/AslTest.jar $dbMachine $dbPort $dbUser $poolSize $expectedConnectionsFirst 2>&1 > /tmp/$remoteUserName.server.out " &
pids="$pids $!"
# Wait for the server to start up
echo -ne "  Waiting for the server to start up..."
sleep 1
while [ `ssh $remoteUserName@$serverFirst "cat /tmp/$remoteUserName.server.out | grep 'Server listening' | wc -l"` != 1 ]
do
        sleep 1
done
echo "OK"

# Run second server
echo "  Starting the second server"
ssh $remoteUserName@$serverSecond "java -jar $remoteHome/AslTest.jar $dbMachine $dbPort $dbUser $poolSize $expectedConnectionsSecond 2>&1 > /tmp/$remoteUserName.server.out " &
pids="$pids $!"
# Wait for the server to start up
echo -ne "  Waiting for the server to start up..."
sleep 1
while [ `ssh $remoteUserName@$serverSecond "cat /tmp/$remoteUserName.server.out | grep 'Server listening' | wc -l"` != 1 ] 
do
  sleep 1
done
echo "OK"

echo "  Start the clients on the client machine: $clientMachine"
# Run RandomClients
if [ $noRandomClients -ne 0 ];
then
    ssh $remoteUserName@$clientMachine "cd $remoteHome; java -cp AslTest.jar client.RandomClientLaunchMultipleServers $serverFirst $serverSecond 3456 $noRandomClients $clientRunTime true $waitInterval $pollInterval 1 $messageSize 2>&1 > /tmp/out.$remoteUserName.RandomClientLauncherMultipleServers" &
    pids="$pids $!"

    # Check that all random clients have been launched
    echo -ne " Waiting for all random clients to be launched..."
    sleep 1
    while [ `ssh $remoteUserName@$clientMachine "cat /tmp/out.$remoteUserName.RandomClientLauncherMultipleServers | grep 'RandomClientLauncherMultipleServers finished launching ' | wc -l"` != 1 ]
    do
        sleep 1
    done
    echo "OK"
fi;

if [ $noPairClients -ne 0 ]
then
pairClientStart=`expr $noRandomClients + 1`
# Run PairClients
ssh $remoteUserName@$clientMachine "cd $remoteHome; java -cp AslTest.jar client.PairClientLaunch $serverMachine 3456 $noPairClients $clientRunTime $waitInterval $pollInterval true $pairClientStart > /tmp/out.$remoteUserName.PairClientLauncher" &
pids="$pids $!"

# Check that all pair clients have been launched
echo -ne " Waiting for all pair clients to be launched..."
sleep 1
while [ `ssh $remoteUserName@$clientMachine "cat /tmp/out.$remoteUserName.PairClientLauncher | grep 'PairClientLauncher finished launching ' | wc -l"` != 1 ]
do
    sleep 1
done
echo "OK"
fi;

if [ $noReqClients -ne 0 ]
then
# Run ReqResp Clients
ssh $remoteUserName@$clientMachine "cd $remoteHome; java -cp AslTest.jar client.ReqRespClientLaunch $serverMachine 3456 $noReqClients $noRepClients $clientRunTime $waitInterval $pollInterval true > /tmp/out.$remoteUserName.ReqRespClientLauncher" &
pids="$pids $!"

# Check that all ReqResp clients have been launched
echo -ne " Waiting for all ReqResp clients to be launched..."
sleep 1
while [ `ssh $remoteUserName@$clientMachine "cat /tmp/out.$remoteUserName.ReqRespClientLauncher | grep 'ReqRespClientLauncher finished launching ' | wc -l"` != 1 ]
do
        sleep 1
done
echo "OK"
fi;

# Wait for the clients and server to finish
echo -ne "  Waiting for the clients and server to finish ... "
for f in $pids
do
        wait $f
done
echo "OK"

# Make a directory on /mnt/asl for logs
echo "Creating log directory for experiment $experimentId"
logdir="exp"
logdir+=$experimentId
ssh $remoteUserName@$clientMachine "mkdir -p /mnt/asl/$remoteUserName/$logdir/server1"
ssh $remoteUserName@$clientMachine "mkdir -p /mnt/asl/$remoteUserName/$logdir/server2"

echo "Copying logs on client..."
ssh $remoteUserName@$clientMachine "mv /tmp/client* /mnt/asl/$remoteUserName/$logdir"
echo "Copying logs on server1..."
ssh $remoteUserName@$serverFirst "mv /tmp/serverThread* /mnt/asl/$remoteUserName/$logdir/server1"
echo "Copying logs on server2..."
ssh $remoteUserName@$serverSecond "mv /tmp/serverThread* /mnt/asl/$remoteUserName/$logdir/server2"

echo "Stopping database server on DB machine"
ssh $remoteUserName@$dbMachine "cd ~/scripts/db/; ./postgresql stop;"
echo "Copying logs on DB machine..."
ssh $remoteUserName@$dbMachine "mv ~/db/pgsql/data/pg_log/postgresql* /mnt/asl/$remoteUserName/$logdir"



echo "Done"
