#!/usr/bin/python
import sys
import getopt

waitInterval = 500

def processFile(FILENAME, OUTFILE):
    print "Processing " + FILENAME
    respFile = open(OUTFILE, 'w')
    f = open(FILENAME)
    sendTimeLine = f.readline()
    while sendTimeLine != '':
        fields = sendTimeLine.split(None, 3)
        sendTime = fields[2]

        thinkTimeLine = f.readline()
        if thinkTimeLine != '':
           thinkTimeLineFields = thinkTimeLine.split(None, 3);
           thinkTime = thinkTimeLineFields[2]
           timestamp = thinkTimeLineFields[0]

           responseTime = waitInterval - int(thinkTime) - int(sendTime)
           respFile.write(str(timestamp) + " " + str(responseTime) + "\n")

        sendTimeLine = f.readline()

    f.close()
    respFile.close()

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "f:")
    except getopt.GetoptError:
        print 'Usage: responseTime.py -f <filename>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-f':
            print "Argument passed is " + arg
            FILENAME = str(arg)
            OUTFILENAME = "resp." + FILENAME
            
    print FILENAME + OUTFILENAME
    processFile(FILENAME, OUTFILENAME)

if __name__ == "__main__":
    main(sys.argv[1:])
