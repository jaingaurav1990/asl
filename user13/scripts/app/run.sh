#!/bin/bash

###############################################
#
# 2 Hour Trace
# 50 random clients, 16 pair clients, 16 ReqResp clients
# poolSize 30 pollInterval = 50 waitInterval = 500
#
###############################################
./experiment.sh --serverMachine 10.111.1.46 --poolSize 30 --clientMachine 10.111.1.47 --waitInterval 500 --pollInterval 50 --noRandomClients 50 --noPairClients 8 --noReqClients 8 --noRepClients 8 --remoteUserName user13 --experimentId 1 --clientRunTime 7200 --dbMachine 10.111.1.45 --dbPort 44779 --dbUser user13

# Experiment 2
################################################
#
# 50 requesters, 50 servers with a poolSize = 1
# Experiment duration: 20 minutes
#
###############################################
./experiment.sh --serverMachine 10.111.1.46 --poolSize 1 --clientMachine 10.111.1.47 --waitInterval 500 --pollInterval 50 --noRandomClients 0 --noPairClients 0 --noReqClients 50 --noRepClients 50 --remoteUserName user13 --experimentId 2 --clientRunTime 1200 --dbMachine 10.111.1.45 --dbPort 44779 --dbUser user13

# Experiment 3
###############################################
#
# 50 requesters, 50 servers with poolsize = 50
# Experiment ran for 15 minutes
#
###############################################
./experiment.sh --serverMachine 10.111.1.46 --poolSize 50 --clientMachine 10.111.1.47 --waitInterval 500 --pollInterval 50 --noRandomClients 0 --noPairClients 0 --noReqClients 50 --noRepClients 50 --remoteUserName user13 --experimentId 3 --clientRunTime 900 --dbMachine 10.111.1.45 --dbPort 44779 --dbUser user13
