#!/bin/bash

# List all experiment directories
expdirs=`find . ! -path . -maxdepth 1 -type d`
for dir in $expdirs
do
    cd $dir
    echo "Stepped into $dir"

    rm -f client.jar

    clientFiles=`ls client*`
    for file in $clientFiles
    do
        grep -h -i "thinktime" -B 1 $file | grep -v -i "thinktime" | grep -v '\-\-' | awk -F: '{print $1" "$3}' > out.$file
    done

    # Formatted files are now in the format
    # Timestamp     getMessageTime
    #
    #######################################
    formattedFiles=`ls out.*`
    sort -n -t " " -k1,1 $formattedFiles > actualResponseTime.out
    sort -n -c actualResponseTime.out

    rm -f out.*
    # Subtract the value in very first row of very first column  

    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' actualResponseTime.out > actualResponseTime.plot

    maxRespTime=`cat actualResponseTime.plot | awk '{print $2}' | sort -n -r | head -1`
    workingDir=`echo $dir | cut -b3-`
    id=`echo $workingDir | grep -o '[0-9]*'`

    cd ..
    # grep for the parameters for this particular experiment from the summary file
    parameters=`grep -w -A 1 "Experiment $id" SUMMARY | grep -v '\-\-' | tail -1 | grep -o '[0-9]*'`
    numClients=`echo $parameters | awk '{print $1}'`
    numConnections=`echo $parameters | awk '{print $2}'`
    msgSize=`echo $parameters | awk '{print $3}'`
    ./plotResponseTime.r $workingDir $id $numClients $numConnections $msgSize

done

# Now there exists a short respSummary.txt file in each of the experiment
# directories. Merge all of them and put it in a format that can
# be read by R

summaryFiles=`find . -name respSummary.txt`
sort -n --merge $summaryFiles > respOverallSummary.tmp
numLines=`cat respOverallSummary.tmp | wc -l`
echo $numLines
numLines=`expr $numLines / 2 + 1`
cat respOverallSummary.tmp | tail -$numLines > respOverallSummary.txt
rm respOverallSummary.tmp

echo "Generating response time summary..."
./summarizeResponseTime.r

