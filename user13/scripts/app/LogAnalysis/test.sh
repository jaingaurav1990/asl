#!/bin/bash

while getopts ":d:" opt; do
    case $opt in
        d)
            echo "Parameter: $OPTARG"
            find "$OPTARG" -type d
            ;;
        \?)
            echo "Invalid option"
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires argument"
            exit 1
            ;;
    esac
done
