#!/usr/bin/Rscript

# This script works in conjunction with breakdownServerTime.sh
# It reads the files connectionTake.plot, executeQuery.plot and
# read.plot and write the mean and deviation for each of these
# operations in a single file (with two lines: header and actual
# data)
args <- commandArgs(TRUE);
setwd(args[1])
connectionTakeTime = read.table("connectionTake.plot", head = F, sep = ' ')
executeQueryTime = read.table("executeQuery.plot", head = F, sep = ' ')
readTime = read.table("read.plot", head = F, sep = ' ')

lastValidTimeStamp = (connectionTakeTime$V1[nrow(connectionTakeTime)])/1000 - 100
connectionStatData = subset(connectionTakeTime, (connectionTakeTime$V1)/1000 > 100 & (connectionTakeTime$V1)/1000 < lastValidTimeStamp) 
connectionTakeAvg = mean(connectionStatData$V2)
connectionTakeSD = sd(connectionStatData$V2)

lastValidTimeStamp = (executeQueryTime$V1[nrow(executeQueryTime)])/1000 - 100 
executeQueryStatData = subset(executeQueryTime, (executeQueryTime$V1)/1000 > 100 & (executeQueryTime$V1)/1000 < lastValidTimeStamp)               
executeQueryAvg = mean(executeQueryStatData$V2)                                                                                                       
executeQuerySD = sd(executeQueryStatData$V2) 

lastValidTimeStamp = (readTime$V1[nrow(readTime)])/1000 - 100                                                                         
readStatData = subset(readTime, (readTime$V1)/1000 > 100 & (readTime$V1)/1000 < lastValidTimeStamp)                   
readAvg = mean(readStatData$V2)                                                                                                       
readSD = sd(readStatData$V2) 

d = data.frame(ExperimentID = c(as.numeric(args[2])),
               clients = c(as.numeric(args[3])), 
               connections = c(as.numeric(args[4])),
               msgSize = c(as.numeric(args[5])),
               MeanConnectionTake = c(connectionTakeAvg),
               MeanExecuteQuery = c(executeQueryAvg),
               MeanRead = c(readAvg),
               ConnectionTakeSD = c(connectionTakeSD),
               ExecuteQuerySD = c(executeQuerySD),
               ReadSD = c(readSD))

write.table(d, file = "breakDownServerTime.txt", row.names = FALSE)
