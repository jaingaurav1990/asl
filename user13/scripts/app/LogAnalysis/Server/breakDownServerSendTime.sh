#!/bin/bash

#set -x
# Save the directory from which script was launched
currentDir=`pwd`
# Parse options
rootDir=''
scenarioFile=''
resultsDir=''
logDirSpecified=false
scenarioFileSpecified=false
resultsDirSpecified=false

while getopts ":d:s:r:" opt; do
    case $opt in
        d)
            rootDir="$OPTARG"
            logDirSpecified=true
            ;;
        s)
            scenarioFile="$OPTARG"
            scenarioFileSpecified=true
            ;;
        r)
            resultsDir="$OPTARG"
            resultsDirSpecified=true
            ;;
        /?)
            echo "Invalid option"
            exit 1
            ;;
        :)
            echo "Option $opt requires argument"
            exit 1
            ;;
    esac
done

if ! $logDirSpecified || ! $scenarioFileSpecified || ! $resultsDirSpecified
then
    echo "Usage: `basename $0` -d <Directory containing logs> -s <File containing description of all scenarios> \
        -r <Directory which would store results"
    exit 1
fi;

echo "Processing root Log directory $rootDir with scenario summary in $scenarioFile"

# List all experiment directories
expdirs=`find $rootDir -maxdepth 1 -type d ! -path $rootDir`
for dir in $expdirs
do
    cd $dir
    echo "Stepped into $dir"

    serverFiles=`ls serverThread*`
    for file in $serverFiles
    do
        # Extract data for 'time take to acquire connection'
        grep -h -i -w "sendMessage.connectionPool.take()" $file | awk -F: '{print $1" "$3}' > out.connectionTake.$file

        # Extract data for 'time taken to con.executeQuery()'
        grep -h -i -w "sendOne.executeQuery()" $file | awk -F: '{print $1" "$3}' > out.executeQuery.$file

        # Extract data for 'time taken to complete send operation at Server'
        grep -h -i -w "send:" $file | awk -F: '{print $1" "$3}' > out.read.$file
    done

    # Formatted files are now in the format
    # Timestamp     value
    #
    #######################################

    # First arrange files having 'time taken to acquire a connection'
    formattedFiles=`ls out.connectionTake*`
    sort -n -t " " -k1,1 $formattedFiles > connectionTake.out
    sort -n -c connectionTake.out
    rm -f out.connectionTake*

    # Do the same for files having 'time taken for con.executeQuery()'
    formattedFiles=`ls out.executeQuery*`                                                                                                           
    sort -n -t " " -k1,1 $formattedFiles > executeQuery.out                                                                                         
    sort -n -c executeQuery.out
    rm -f out.executeQuery*

    # Do the same for files having 'time taken to complete send operation at server'
    formattedFiles=`ls out.read*`                                                                                                           
    sort -n -t " " -k1,1 $formattedFiles > send.out                                                                                         
    sort -n -c send.out
    rm -f out.send*

    # Subtract the value in very first row of very first column  

    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' connectionTake.out > connectionTake.plot
    rm connectionTake.out

    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' executeQuery.out > executeQuery.plot                                          
    rm executeQuery.out

    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' send.out > send.plot                                          
    rm send.out

    # maxRespTime=`cat actualResponseTime.plot | awk '{print $2}' | sort -n -r | head -1`
    # workingDir=`echo $dir | cut -b3-`
    id=`echo $dir | grep -o '[0-9]*'`

    cd $currentDir
    # grep for the parameters for this particular experiment from the summary file
    parameters=`grep -w "^$id" "$scenarioFile"`
    numClients=`echo $parameters | awk '{print $10}'`
    numConnections=`echo $parameters | awk '{print $8}'`
    msgSize=`echo $parameters | awk '{print $9}'`
    ./plotBreakDownServerSendTime.r $dir $id $numClients $numConnections $msgSize

done

# In the following, assumption is that we are in the root directory containing experiment logs
# after the above for loop finishes
cd $rootDir

# Now there exists a short breakDownServerSendTime.txt file in each of the experiment
# directories. Merge all of them and put it in a format that can
# be read by R

# First switch to root dir containing experiment logs
summaryFiles=`find . -name breakDownServerSendTime.txt`
sort -n --merge $summaryFiles > breakDownServerSendTimeOverall.tmp
numLines=`cat breakDownServerSendTimeOverall.tmp | wc -l`
#echo $numLines
numLines=`expr $numLines / 2 + 1`
cat breakDownServerSendTimeOverall.tmp | tail -$numLines > breakDownServerSendTimeSummary.txt
rm breakDownServerSendTimeOverall.tmp
cd $currentDir
mv "$rootDir/breakDownServerSendTimeSummary.txt" $resultsDir
echo "Generated break down of 'send' operation time spent in server in breakDownServerSendTimeSummary.txt in $resultsDir ..."

#echo "Generating response time summary..."
#./summarizeResponseTime.r
