#!/usr/bin/Rscript

args <- commandArgs(TRUE);
# args[1] Working directory
# args[2] Experiment ID
# args[3] Number of Clients
# args[4] Number of connections (to database)
# args[5] Message size in bytes

setwd(args[1]);
plotData <- read.table('clientThroughput.plot', head = F, sep = ' ');

g_range <- range(0, plotData$V2);
lastValidTimeStamp = (plotData$V1[nrow(plotData)])/1000 -100 
statData = subset(plotData, (plotData$V1)/1000 > 100 & (plotData$V1)/1000 < lastValidTimeStamp )
myAvg = mean(statData$V2)
myMedian = median(statData$V2)
myDeviation = sd(statData$V2)
error = qt(0.975, df = length(statData$V2) - 1)*myDeviation/sqrt(length(statData$V2));
PercentageError = (error/myAvg)*100;

# Create a table with single row that summarizes
# the result of this particular experiment
d = data.frame(ExperimentID = c(as.numeric(args[2])),
             clients = c(as.numeric(args[3])), 
             connections = c(as.numeric(args[4])),
             msgSize = c(as.numeric(args[5])),
             Mean = c(myAvg),
             Deviation = c(myDeviation),
             Error = c(PercentageError))

write.table(d, file = "throughputSummary.txt", row.names = FALSE)

idx = seq(1, length(plotData$V1), 1)
plotData$V1 = plotData$V1/1000;
ts = plotData$V1[idx];
throughput = plotData$V2[idx];
png(filename=paste("allClientThroughput",".",args[2],".png", sep = ""), height=695, width=700, bg="white");
plot(ts, throughput, ylim = g_range, ann = FALSE)

# Create box around plot
box()
text(1.5, y = NULL, paste("Mean =", round(myAvg, 1), "\nMedian =", 
                              round(myMedian, 1), "\nStd.Dev =", round(myDeviation, 1), 
                              "\nPercentage Error =", round(PercentageError, 1)), pos = 4)

# Label the x and y axes with dark green text
title(xlab = "Time (in seconds)", col.lab = rgb(0, 0.5, 0))
title(ylab = "Throughput (in messages/second)", col.lab = rgb(0, 0.5, 0))
title(main= paste("Experiment ",args[2]), col.main="red", font.main=4)
