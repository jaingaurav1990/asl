#!/usr/bin/python

import sys, getopt

def processFile(outfile, infile, interval):
    print "Processing " + infile
    fileEnd = False
    throughputFile = open(outfile, 'w')
    f = open(infile)
    counter = interval
    numMessages = 0
    while fileEnd == False:
        line = f.readline()
        if line == '':
            break
        fields = line.split(None, 2)
        ts = fields[0]
        if int(ts) <= counter:
            numMessages = numMessages + 1
        else:
            throughput = (numMessages)/((interval)/1000)
            throughputFile.write(str(counter) + " " + str(throughput) + "\n")
            counter = counter + interval
            numMessages = 1

    f.close()
    throughputFile.close()

def main(argv):
    try: 
        opts, args = getopt.getopt(argv, "i:f:")
    except getopt.GetoptError:
        print 'Usage: clientThroughput.py -i <interval> -f <tempThroughputFile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-f':
            infile = arg
        if opt == '-i':
            interval = arg

    processFile("clientThroughput.plot", infile, int(interval))

if __name__ == "__main__":
    main(sys.argv[1:])





    

