#!/usr/bin/python

import sys, getopt

def processFile(outfile, infile, start_ts, duration, numClients, numConnections, msgSize, expID):
    print "Processing " + infile
    fileEnd = False
    throughputFile = open(outfile, 'w')
    f = open(infile)
    end_ts = start_ts + duration
    numMessages = 0.0
    while fileEnd == False:
        line = f.readline()
        if line == '':
            break
        fields = line.split(None, 2)
        ts = fields[0]
        if int(ts) < start_ts or int(ts) > end_ts:
            " Ignore all lines before start_ts"
            pass
        else:
            numMessages = numMessages + 1

    throughput = (numMessages)/((duration)/1000.0)
    " Write header in macroThroughput file"
    throughputFile.write("ExperimentID Clients Connections MsgSize MacroThroughput\n")
    throughputFile.write(str(expID) + " " + str(numClients) + " " + str(numConnections) + " " + str(msgSize) + " " + str(throughput) + "\n")

    f.close()
    throughputFile.close()

def main(argv):
    try: 
        opts, args = getopt.getopt(argv, "i:f:s:d:c:n:m:")
    except getopt.GetoptError:
        print 'Usage: macroThroughput.py -i <expID> -c <numClients> -n <numConnections> -m <msgSize> -s <start-timestamp (in seconds)> -d <Experiment duration> -f <tempThroughputFile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-i':
            expID = arg
        if opt == '-f':
            infile = arg
        if opt == '-s':
            start_ts = arg
        if opt == '-d':
            duration = arg
        if opt == '-c':
            numClients = arg
        if opt == '-n':
            numConnections = arg
        if opt == '-m':
            msgSize = arg

    processFile("macroThroughputSummary.txt", infile, int(start_ts) * 1000.0, int(duration) * 1000.0, int(numClients), int(numConnections), int(msgSize), int(expID))

if __name__ == "__main__":
    main(sys.argv[1:])
