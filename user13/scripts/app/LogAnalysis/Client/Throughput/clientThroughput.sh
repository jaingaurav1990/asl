#!/bin/bash

# Calculate throughput as seen by the clients

# Save the directory from which script was launched
currentDir=`pwd`
# Parse options
rootDir=''
scenarioFile=''
resutlsDir=''
logDirSpecified=false
scenarioFileSpecified=false
resultsDirSpecified=false

while getopts ":d:s:r:" opt; do
    case $opt in
        d)
            rootDir="$OPTARG"
            logDirSpecified=true
            ;;
        s)
            scenarioFile="$OPTARG"
            scenarioFileSpecified=true
            ;;
        r)
            resultsDir="$OPTARG"
            resultsDirSpecified=true
            ;;
        /?)
            echo "Invalid option"
            exit 1
            ;;
        :)
            echo "Option $opt requires argument"
            exit 1
            ;;
    esac
done

if ! $logDirSpecified || ! $scenarioFileSpecified || ! $resultsDirSpecified
then
    echo "Usage: `basename $0` -d <Directory containing logs> -s <File containing description of all scenarios>"
    exit 1
fi;

echo "Processing root Log directory $rootDir with scenario summary in $scenarioFile"

expdirs=`find $rootDir -maxdepth 1 -type d ! -path $rootDir`
for dir in $expdirs
do
    cd $dir
    echo "Stepped into $dir"

    rm -f client.jar

    clientFiles=`ls clientThread*`
    for file in $clientFiles
    do
        grep -h -i "\$5" $file | awk -F: '{print $1""$3}' > out.$file
    done

    formattedFiles=`ls out.*`
    sort -n -t " " -k1,1 $formattedFiles > clientThroughput.out
    sort -n -c clientThroughput.out

    rm out.*

    # Subtract the value in very first row of very first column  
    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' clientThroughput.out > absTimeClientThroughput.out

    pythonScript="$currentDir"
    pythonScript+="/clientThroughput.py"
    python $pythonScript -i 1000 -f absTimeClientThroughput.out
 
    id=`echo $dir | grep -o '[0-9]*'`

    cd $currentDir
    parameters=`grep -w "^$id" "$scenarioFile"`
    numClients=`echo $parameters | awk '{print $10}'`
    numConnections=`echo $parameters | awk '{print $8}'`
    msgSize=`echo $parameters | awk '{print $9}'`

    ./plotThroughput.r $dir $id $numClients $numConnections $msgSize

done

# Now there exists a short throughputSummary.txt file in each of the experiment
# directories. Merge all of them and put it in a format that can
# be read by R
cd $rootDir
summaryFiles=`find . -name throughputSummary.txt`
sort -n --merge $summaryFiles > throughputOverallSummary.tmp
numLines=`cat throughputOverallSummary.tmp | wc -l`
numLines=`expr $numLines / 2 + 1`
cat throughputOverallSummary.tmp | tail -$numLines > ThroughputOverallSummary.txt
rm throughputOverallSummary.tmp
cd $currentDir
mv "$rootDir/ThroughputOverallSummary.txt" $resultsDir 
echo "Generated ThroughputOverallSummary.txt in $resultsDir" 
#echo "Generating throughput summary..."
#./summarizeThroughput.r

