#!/bin/bash

set -x
# Save the directory from which script was launched
currentDir=`pwd`
# Parse options
rootDir=''
scenarioFile=''
resultsDir=''
logDirSpecified=false
scenarioFileSpecified=false
resultsDirSpecified=false

while getopts ":d:s:r:" opt; do
    case $opt in
        d)
            rootDir="$OPTARG"
            logDirSpecified=true
            ;;
        s)
            scenarioFile="$OPTARG"
            scenarioFileSpecified=true
            ;;
        r)
            resultsDir="$OPTARG"
            resultsDirSpecified=true
            ;;
        /?)
            echo "Invalid option"
            exit 1
            ;;
        :)
            echo "Option $opt requires argument"
            exit 1
            ;;
    esac
done

if ! $logDirSpecified || ! $scenarioFileSpecified || ! $resultsDirSpecified
then
    echo "Usage: `basename $0` -d <Directory containing logs> -s <File containing description of all scenarios> \
        -r <Directory which would store results"
    exit 1
fi;

echo "Processing root Log directory $rootDir with scenario summary in $scenarioFile"

# List all experiment directories
expdirs=`find $rootDir -maxdepth 1 -type d ! -path $rootDir`
for dir in $expdirs
do
    cd $dir
    echo "Stepped into $dir"

    rm -f client.jar
    rm out.*

    clientFiles=`ls clientThread*`
    for file in $clientFiles
    do
        grep -h -i "thinktime" -B 1 $file | grep -v -i "thinktime" | grep -v '\-\-' | awk -F: '{print $1" "$3}' > out.$file
    done

    # Formatted files are now in the format
    # Timestamp     get/Read MessageTime
    #
    #######################################
    formattedFiles=`ls out.*`
    sort -n -t " " -k1,1 $formattedFiles > actualResponseTime.out
    sort -n -c actualResponseTime.out

    rm -f out.*
    # Subtract the value in very first row of very first column  

    awk '{ if (NR == 1) { shift = $1 } print ($1 - shift) " " $2 }' actualResponseTime.out > actualResponseTime.plot

    # maxRespTime=`cat actualResponseTime.plot | awk '{print $2}' | sort -n -r | head -1`
    # workingDir=`echo $dir | cut -b3-`
    id=`echo $dir | grep -o '[0-9]*'`

    cd $currentDir
    # grep for the parameters for this particular experiment from the summary file
    parameters=`grep -w "^$id" "$scenarioFile"`
    numClients=`echo $parameters | awk '{print $10}'`
    numConnections=`echo $parameters | awk '{print $8}'`
    msgSize=`echo $parameters | awk '{print $9}'`
    ./plotResponseTime.r $dir $id $numClients $numConnections $msgSize

done

# In the following, assumption is that we are in the root directory containing experiment logs
# after the above for loop finishes
cd $rootDir

# Now there exists a short respSummary.txt file in each of the experiment
# directories. Merge all of them and put it in a format that can
# be read by R

set -x
# First switch to root dir containing experiment logs
summaryFiles=`find . -name respSummary.txt`
sort -n --merge $summaryFiles > respOverallSummary.tmp
numLines=`cat respOverallSummary.tmp | wc -l`
#echo $numLines
numLines=`expr $numLines / 2 + 1`
cat respOverallSummary.tmp | tail -$numLines > ResponseTimeOverallSummary.txt
rm respOverallSummary.tmp
cd $currentDir
mv "$rootDir/ResponseTimeOverallSummary.txt" $resultsDir
echo "Generated Response Time overall summary in ResponseTimeOverallSummary.txt in $resultsDir ..."

#echo "Generating response time summary..."
#./summarizeResponseTime.r

