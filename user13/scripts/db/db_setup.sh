#!/bin/bash

# Use 'trust' authentication method in pg_hba.conf file for this
# script to not prompt for password every now and then
# Add following to your pg_hba.conf
# local all all trust
set -x
sqlfile="DBscheme.sql"

postgres_path=$HOME
postgres_path+="/scripts/db/postgresql"

export PGUSER=${PGUSER-${USER}}
export PGDATABASE=${PGDATABASE-asl_hs13_g13}
export PGPORT="44779"
export DEBUG=${DEBUG-1}

# Start Postgres data server
$postgres_path start
#$postgres_path restart
#$postgres_path reload # For some weird reason, restart ain't enough

# Check if the role ${PGUSER} exists or not
createuser --createdb --no-createrole --no-superuser --no-password ${PGUSER} 2>/dev/null

psql -p $PGPORT -U $PGUSER ${PGDATABASE} -c '\q' 2>&1
if [ $? -eq 0 ]
then
    dropdb ${PGDATABASE}
fi

createdb -O ${PGUSER} ${PGDATABASE}

psql \
    -X \
    -f $sqlfile \
    -h localhost \
    --echo-all \
    --set AUTOCOMMIT=off \
    --set ON_ERROR_STOP=on \
    
status=$?
if [ "$status" -ne 0 ]
then
    echo "ERROR executing psql with file $sqlfile"
    exit $status
fi

echo "DATABASE successfully set up....Proceed."
exit 0
